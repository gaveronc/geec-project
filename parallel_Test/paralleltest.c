/* PC0 to PC11 -> Audio data */

/* PB1 -> Return bit*/
/* PB5 -> Reset*/
/* PB6 -> Load bit*/

#include "stm32f10x.h"
#include <stdint.h>
#include "paralleltest.h"

void gpioInit(void) {
	//Enable peripheral clock for Port B and Alternate function

	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN| RCC_APB2ENR_AFIOEN;
	

	
	//external interrupt for PB1
	AFIO->EXTICR[1] |= (1<<3);
	

	
	//Port B -> General purpose output Open-drain and Output mode (max speed of 50MHz)
	GPIOC->CRL |=0x77777777;
	GPIOC->CRH |=0x00007777;
	//GPIOB->CRL |=0x00700077;
	//GPIOC->CRH |=0x00007777;
	GPIOB->CRL |=0x03300040;

}


uint16_t parallelComm(uint16_t audiodata, uint16_t *datafromFPGA){

	
	//1st clock
	
	GPIOC->ODR = audiodata;
	GPIOB->ODR = receiveControl;

	//2nd clock
	GPIOC->ODR = audiodata;
	GPIOB->ODR= loadControl;


	//3rd clock
	GPIOB->ODR = receiveControl;
	GPIOC->ODR = 0xFFF;

	while(GPIOB->IDR & (1<<1)) 
	{
		
	}
	//4th clock
	*datafromFPGA=(GPIOC->IDR & 0xFFF);
	//5th clock
	GPIOC->ODR = 0xFFF;
	GPIOB->ODR = resetControl; 

	//6th clock
	GPIOC->ODR = 0xFFF;
	GPIOB->ODR = receiveControl;

	
	
}
