#include "stm32f10x.h"
#include <stdint.h>

void systick_init(void);
void ADC_init(void);
void ADC_start(uint32_t channel);
uint16_t ADC_read(void);

