#include "stm32f10x.h"
#include <stdint.h>
#include "dac.h"

void dacInit (void){
	
	//Bit 29 (DAC interface clock enable)
	RCC->APB1ENR |= RCC_APB1ENR_DACEN;
	
	//Bit 2 (I/O Port A clock enable) for output
	RCC->APB2ENR |= 1<<2;
	
	//Port A -> Alternate function output Push-pull and output mode max speed of 50MHz
	GPIOA->CRL |= GPIO_CRL_MODE4 | GPIO_CRL_CNF4_1;
	GPIOA->CRL &= ~GPIO_CRL_CNF4_0;
	
	//DAC channel 1 enable
	DAC->CR |= 1<<0;
}

void dacWrite (uint16_t value)
{
	//write the value in 12-bit right aligned data
	DAC->DHR12R1 =value;
}
