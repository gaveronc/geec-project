#include "stm32f10x.h"
#include <stdint.h>

#define	receiveControl (0x62);
#define loadControl (0x22);
#define resetControl (0x42);


void gpioInit (void);
uint16_t parallelComm (uint16_t, uint16_t *);
