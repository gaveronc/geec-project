#include "stm32f10x.h"
#include <stdint.h>
#include "adc.h"

void systick_init(void)
{
	
	// SysTick reset, initialization and enable.
	//24MHz chosen as clocksource
	SysTick->CTRL = 0;
	SysTick->VAL = 0;
	SysTick->CTRL = 0x7;	// Enables the counter.
	SysTick->LOAD = 542; 

}
void ADC_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN  |RCC_APB2ENR_ADC1EN;//Enable ADC1 clock 

	//PA1 as audio input
	GPIOA->CRL &= ~(GPIO_CRL_MODE1);
	GPIOA->CRL &= ~(GPIO_CRL_CNF1);
	
	//PC0, PC1, PC2, PC3, PC4, PC5 as control data
	GPIOC->CRL &= ~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 |GPIO_CRL_MODE2 |GPIO_CRL_MODE3 |GPIO_CRL_MODE4 | GPIO_CRL_MODE5);
	GPIOC->CRL &= ~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2 | GPIO_CRL_CNF3 | GPIO_CRL_CNF4 | GPIO_CRL_CNF5);
	
	GPIOB->CRL &= ~0x0000000F;
	
	ADC1->CR2 |= ADC_CR2_ADON;//Turn on ADC1
	ADC1->SMPR2 |= 0x00000000;	// sample time of 1.5 cycles for audio
	//ADC1->SMPR2 |= 0x0000028; //55.5 cycles
	//ADC1->SMPR2 |= 0x0000038;	//239.5 cycles
	ADC1->SMPR1 |= (1<<0) | (1<<3) | (1<<6)| (1<<9) | (1<<12) | (1<<15); //sample time of 7.5 cycles for control data
	
	ADC1->SQR1 &= ~ADC_SQR1_L;//Only one conversion in sequence
	ADC1->SQR3 &= 0x0;//Channel zero
}



void ADC_start(uint32_t channel)
{
	ADC1->SQR3 = channel;//Select channel to convert
	ADC1->CR2 |= ADC_CR2_ADON;//Start conversion
}

uint16_t ADC_read(void)
{
	while((ADC1->SR & ADC_SR_EOC) == 0); // Wait for EOC
	return ADC1->DR; // Return converted value
}

	
	

