#include "stm32f10x.h"
#include <stdint.h>
#include "spi.h"

/*
This module is configured for the following settings:
CPOL=1
CPHA=0
Bidirectional Data
16-bit frames
MSB First
*/
void clockInit (void)
{ 
	//GPIOA enable (Bit 2)
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	//SPI1 enable (Bit 12)
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	//SPI2 enable (Bit 14)
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
}
void spiInit (void)
{
	/* for audio data*/
	
	GPIOA->CRL &= 0x000F0FFF;
	//MOSI,MISO,NSS,SCLK
	GPIOA->CRL |= 0xB4B03000;
	//set PA3(Chip Select) high
	GPIOA->ODR |= 1 << 3;
	//Reset SPI1 settings
	SPI1->CR1 = 0x00000000; 
	SPI1->CR2 = 0x00000000;
	
	//enbale slave management (Bit 9)
	SPI1->CR1 |= SPI_CR1_SSM;
	
	//Internal slave select (Bit 8)
	SPI1->CR1 |= SPI_CR1_SSI;
	
	//Master Selection (Bit 2)
	SPI1->CR1 |= SPI_CR1_MSTR;
	
	//16-bit data frame format is selected for transmission/reception (Bit 11)
	SPI1->CR1 |= SPI_CR1_DFF;
	
	//CPOL=1 (Bit 1)
	SPI1->CR1 |= SPI_CR1_CPOL;
	
	//SPI enable (Bit 6)
	SPI1->CR1 |= SPI_CR1_SPE;
		
	//fastest Baud rate
	SPI1->CR1 &= 0xFFC7;
	
	/* for control data */
	
	GPIOB->CRH &= 0x0000FFFF;
	//MOSI,MISO,NSS,SCLK
	GPIOB->CRH |= 0xB4B30000;
	//set PB12(Chip Select) high
	GPIOB->ODR |= 1 << 12;
	//Reset SPI1 settings
	SPI2->CR1 = 0x00000000; 
	SPI2->CR2 = 0x00000000;
	//enbale slave management (Bit 9)
	SPI2->CR1 |= SPI_CR1_SSM;
	
	//Internal slave select (Bit 8)
	SPI2->CR1 |= SPI_CR1_SSI;
	
	//Master Selection (Bit 2)
	SPI2->CR1 |= SPI_CR1_MSTR;
	
	//16-bit data frame format is selected for transmission/reception (Bit 11)
	SPI2->CR1 |= SPI_CR1_DFF;
	
	//CPOL=1 (Bit 1)
	SPI2->CR1 |= SPI_CR1_CPOL;
	
	//SPI enable (Bit 6)
	SPI2->CR1 |= SPI_CR1_SPE;
	
	//fastest Baud rate
	SPI2->CR1 &= 0xFFC7;
}

uint16_t spiaudioSend (uint16_t msg)
{
	GPIOA->ODR &= ~(1 << 3);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI1->SR & SPI_SR_TXE)){};
	SPI1->DR=msg;
	while(!(SPI1->SR & SPI_SR_RXNE)){};
	
	
	GPIOA->ODR |= 1<<3;
	
	return SPI1->DR;
		
}
/*
uint16_t spicontrolSend (uint16_t msg)
{
	GPIOB->ODR &= ~(1 << 12);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI2->SR & SPI_SR_TXE)){};
	SPI2->DR=msg;
	while(!(SPI2->SR & SPI_SR_RXNE)){};
	
	
	GPIOB->ODR |= 1<<12;
	
	return SPI2->DR;
		
}
*/
/*
uint16_t spicontrolSend (uint16_t msg)
{
	GPIOB->ODR &= ~(1 << 12);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI2->SR & SPI_SR_TXE)){};
	SPI2->DR=msg;
	while(!(SPI2->SR & SPI_SR_RXNE)){};
	
	
	GPIOB->ODR |= 1<<12;
	
	return SPI2->DR;
		
}
*/

uint16_t spicontrolSend (uint16_t value, uint8_t potnum)
{
	GPIOB->ODR &= ~(1 << 12);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI2->SR & SPI_SR_TXE)){};
	SPI2->DR= value |(potnum<<12);
	while(!(SPI2->SR & SPI_SR_RXNE)){};
	
	
	GPIOB->ODR |= 1<<12;
	return SPI2->DR;
	
		
}