#include "stm32f10x.h"
#include <stdint.h>
#include "adc.h"
#include "spi.h"
#include "dac.h"


uint16_t audiovalue;
uint16_t audiodatafromFPGA;
uint16_t controldatafromFGPA;
uint16_t controlvalues; // Store ADC readings
int i = 0;

void SysTick_Handler (void)
{
	static int potnum=1;
	// Start conversion on channel 1
	ADC_start(1);
	audiovalue=ADC_read();
	audiodatafromFPGA=spiaudioSend(audiovalue);
	dacWrite(audiodatafromFPGA & 0xFFF);
	
	ADC_start(potnum+9);
	controlvalues=ADC_read();
	spicontrolSend(controlvalues,potnum);
	if (potnum < 7)
	{
		potnum+=1;
	}
	else potnum=1;
}


int main(void)
{


	
	//int i;

	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN; // Enable all GPIO clocks
	
	// PC12 is master reset. It is configured as general purpose push-pull output and set high.
	GPIOC->CRH &= ~0x000F0000;
	GPIOC->CRH |= 0x00030000;
	GPIOC->ODR = (1<<12);
	
	clockInit();
	ADC_init();
	spiInit();
	dacInit();
	systick_init();

	
	while(1){
/*
		for (i=0; i<6; i++){
				ADC_start(i+10); // Start conversion on channel 10
				controlvalues= ADC_read(); // Get and store converted value7
				spicontrolSend(controlvalues);
			}
	*/
	}
		
}
		

