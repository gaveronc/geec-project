-- Quartus II VHDL Template
-- Binary Counter

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Clk_Div is

	generic
	(
		MIN_COUNT : natural := 0;
		MAX_COUNT : natural := 5000000
	);

	port
	(
		reset_val	: in integer;
		clk		  	: in std_logic;
		reset	  		: in std_logic;
		enable	  	: in std_logic;
		q		  		: out std_logic
	);

end entity;

architecture divider of Clk_Div is
begin

	process (clk)
		variable	cnt : integer range MIN_COUNT to MAX_COUNT;
		variable buf : std_logic := '0';
	begin
		if (rising_edge(clk)) then

			if reset = '1' then
				-- Reset the counter to 0
				cnt := 0;
				buf := '0';

			elsif enable = '1' then
				-- Increment the counter if counting is enabled			   
				cnt := cnt + 1;
				if (cnt = MAX_COUNT or cnt = reset_val) then
					buf := not buf;
					cnt := 0;
				end if;
			end if;
			q <= buf;
		end if;
	end process;

end divider;
