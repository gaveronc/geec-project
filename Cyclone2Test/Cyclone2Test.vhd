--------------------------
--Cyclone 2 Test Project
--To boldy flash LEDs like
--no one has flashed LEDs.
--------------------------

library ieee;
use ieee.std_logic_1164.all;

entity Cyclone2Test is

	port(
		clk		 : in	std_logic;--Goes to master clock
		output	 : out	std_logic_vector(2 downto 0)--Hook up to LEDs
	);

end entity;

architecture rtl of Cyclone2Test is
	
	-- Clock divider
	signal Clk1, Clk2, Clk3, enable, reset : STD_LOGIC;
	signal Set1 : integer;
	
	component Clk_Div
		port(
			reset_val: in integer;
			clk		: in std_logic;
			reset		: in std_logic;
			enable	: in std_logic;
			q			: out std_logic
		);
	end component;

begin

	div1: Clk_Div port map
		(
			reset_val => Set1,
			clk => clk,
			reset => reset,
			enable => enable,
			q => Clk1
		);
		
	div2: Clk_Div port map
		(
			reset_val => Set1,
			clk => clk,
			reset => reset,
			enable => Clk1,
			q => Clk2
		);
	
	div3: Clk_Div port map
		(
			reset_val => Set1,
			clk => clk,
			reset => reset,
			enable => Clk2,
			q => Clk3
		);
	
	Set1 <= 10;
	--Set1 <= 5000000;
	enable <= '1';
	reset <= '0';
	
	output(0) <= Clk1;
	output(1) <= Clk2;
	output(2) <= Clk3;

end rtl;

