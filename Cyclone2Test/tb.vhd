
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test is
end test;

architecture TB of test is
    component Cyclone2Test
    port (
	    clk         : in    std_logic;
        output      : out   std_LOGIC_VECTOR(2 downto 0)
    ); end component;
    
    signal clk      : std_logic := '0';
    signal output   : std_logic_vector(2 downto 0);
    
begin

    clk <= not clk after 1 ns;
    uut: Cyclone2Test PORT MAP (
            clk => clk,
            output => output
        );

    process
    begin
        --clk <= '0';
		wait for 100 us;
		wait;
    end process;

end TB;

