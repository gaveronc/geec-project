-- IO Test program

LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity IOTest is
	port
	(
		-- Input ports
		Inputs	: in  std_logic_vector(4 downto 0); -- Configured with internal pull-up

		-- Output ports
		LEDs	: out std_logic_vector(4 downto 0)
	);
end IOTest;

architecture rtl of IOTest is
begin
	
	LEDs <= Inputs; -- This is all we need to know
	
end rtl;
