-- Synchronize a signal to the core clock by adding a one-tick delay

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity OneTickDelay is
	port (
		async	:	in		std_logic;	-- Unsychronized signal
		clk	:	in		std_logic;	-- Synchronize to this clock signal
		sync	:	out	std_logic	-- Syncronized signal
	);
	
	signal sync_buf	:	std_logic := '0';
end OneTickDelay;

architecture rtl of OneTickDelay is
begin
	process (clk)
		variable count	:	integer range 0 to 1 := 0;
	begin
		if (rising_edge(clk)) then
			if (async /= sync_buf) then
				if (count = 1) then
					sync_buf <= async;
				else
					count := count + 1;
				end if;
			else
				count := 0;
			end if;
		end if;
	end process;
	sync <= sync_buf;
end rtl;