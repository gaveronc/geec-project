-- The main GEEC project file
-- Basically just wires things together

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity FPGAGEEC is
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		MUL_WIDTH		:	natural	:= 12;
		SPI_WIDTH		:	natural	:= 16
	);

	port
	(
		-- Multi-pathed input ports
		reset		: in  std_logic;
		clk		: in  std_logic;
		enables	: in	std_logic_vector(3 downto 0); -- Enable switches

		-- SPI input ports
		ctrl_mosi		: in	std_logic;
		ctrl_cs_n		: in	std_logic;
		ctrl_sclk		: in	std_logic;
		
		-- SPI output ports
		ctrl_miso	: out	std_logic;
		
		-- Audio data ports
		audio_miso	: out std_logic;
		audio_mosi	: in std_logic;
		audio_cs_n	: in std_logic;
		audio_sclk	: in std_logic;
		audio_ready	: out std_logic;
		
		-- LED config
		LEDS	: out std_logic_vector(3 downto 0);
		
		-- Parallel memory device ports
		mem_data		: inout std_logic_vector(15 downto 0);
		mem_address	: out std_logic_vector(17 downto 0);
		mem_ce		: out std_logic;
		mem_oe		: out std_logic;
		mem_we		: out std_logic;
		mem_lb		: out std_logic;
		mem_hb		: out std_logic;
		
		-- Debug pins
		effectStatus	: out	std_logic_vector(2 downto 0);
		debug				: out std_logic_vector(2 downto 0)
	);
end FPGAGEEC;

architecture rtl of FPGAGEEC is
	
	component spi_mux
	  PORT(
		clk			: in std_logic;
		reset			: in std_logic; -- Resets all registers
		ready 		: in  std_logic; -- Indicates spi_data is ready
		spi_data		: in	std_logic_vector((SPI_WIDTH-1) downto 0); -- data from spi module
		outputReg1	: out	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Goes to effect data register
		outputReg2	: out std_logic_vector((EFFECT_WIDTH-1) downto 0);
		outputReg3	: out std_logic_vector((EFFECT_WIDTH-1) downto 0);
		outputReg4	: out std_logic_vector((EFFECT_WIDTH-1) downto 0);
		outputReg5	: out std_logic_vector((EFFECT_WIDTH-1) downto 0);
		outputReg6	: out std_logic_vector((EFFECT_WIDTH-1) downto 0)
	  );
	end component;
	
	type effectRegMux is array (5 downto 0) of std_logic_vector((EFFECT_WIDTH-1) downto 0);
	signal mux_out, mux_sync	: effectRegMux;
	
	component LinearGain
		PORT(
			clk				: in	std_logic; -- Driven by master clock
			input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			clear     		: in	std_logic;
			disable   		: in	std_logic;
			load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
			effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
			effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
			load_out			: out std_logic; -- Indicator for next effect stage
			output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
			-- RAM ports
			RAM_address		: out std_logic_vector(17 downto 0);
			RAM_data_out	: out std_logic_vector(15 downto 0);
			RAM_data_in		: in	std_logic_vector(15 downto 0);
			RAM_rw			: out	std_logic;
			RAM_enable		: out	std_logic;
			RAM_done			: in	std_logic
			
			-- Non-standard debug ports
			--debug				: out	std_logic_vector(2 downto 0)
		);
	end component;
	
	component NonLinearGain
		PORT(
			clk				: in	std_logic; -- Driven by master clock
			input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			clear     		: in	std_logic;
			disable   		: in	std_logic;
			load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
			effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
			effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
			load_out			: out std_logic; -- Indicator for next effect stage
			output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
			-- RAM ports
			RAM_address		: out std_logic_vector(17 downto 0);
			RAM_data_out	: out std_logic_vector(15 downto 0);
			RAM_data_in		: in	std_logic_vector(15 downto 0);
			RAM_rw			: out	std_logic;
			RAM_enable		: out	std_logic;
			RAM_done			: in	std_logic
			
			-- Non-standard debug ports
			--debug				: out	std_logic_vector(2 downto 0)
		);
	end component;
	
	component effectFeedback
		PORT(
			clk				: in	std_logic; -- Driven by master clock
			input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			clear     		: in	std_logic;
			disable   		: in	std_logic;
			load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
			effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
			effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
			load_out			: out std_logic; -- Indicator for next effect stage
			output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
			-- RAM ports
			RAM_address		: out std_logic_vector(17 downto 0);
			RAM_data_out	: out std_logic_vector(15 downto 0);
			RAM_data_in		: in	std_logic_vector(15 downto 0);
			RAM_rw			: out	std_logic;
			RAM_enable		: out	std_logic;
			RAM_done			: in	std_logic
		);
	end component;
	
	component Passthrough
		PORT(
			clk				: in	std_logic; -- Driven by master clock
			input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			clear     		: in	std_logic;
			disable   		: in	std_logic;
			load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
			effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
			effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
			load_out			: out std_logic; -- Indicator for next effect stage
			output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
			-- RAM ports
			RAM_address		: out std_logic_vector(17 downto 0);
			RAM_data_out	: out std_logic_vector(15 downto 0);
			RAM_data_in		: in	std_logic_vector(15 downto 0);
			RAM_rw			: out	std_logic;
			RAM_enable		: out	std_logic;
			RAM_done			: in	std_logic
		);
	end component;
	
	component spi_slave
		PORT (
			 cs_n    : in    std_logic;
			 mosi    : in    std_logic;
			 miso    : out   std_logic;
			 reset_n : in    std_logic;
			 sclk    : in    std_logic;
			 
			 -- Internal signals
			 tx_reg  : in    std_logic_vector(15 downto 0);
			 rx_reg  : out   std_logic_vector(15 downto 0);
			 done    : out   std_logic;
			 
			 -- Debug Signal
			 debug_out : out std_logic_vector(15 downto 0)
		);
	end component;
	
	-- Effect signals
	signal load_stage	:	std_logic_vector(2 downto 0);
	type effect_input_type is array(2 downto 0) of signed(11 downto 0);
	signal effect_input	:	effect_input_type;
	
	-- SPI signals
	signal ctrl_spi_done		:	std_logic;
   signal ctrl_rx_reg   : std_logic_vector((SPI_WIDTH-1) downto 0) := (others => '0');
   signal ctrl_tx_reg   : std_logic_vector((SPI_WIDTH-1) downto 0) := (others => '0');
	
	-- Audio signals
	signal audio_spi_done		:	std_logic;
   signal audio_rx_reg   : std_logic_vector((SPI_WIDTH-1) downto 0) := (others => '0');
   signal audio_tx_reg	: std_logic_vector((SPI_WIDTH-1) downto 0) := (others => '0');
	signal audio_tx_sign, audio_rx_sign	: signed(11 downto 0);
	
	-- Multiplier signals
	signal e1mul1, e2mul2	: signed	((MUL_WIDTH-1) downto 0);
	signal e1mulOut	: signed ((2*MUL_WIDTH-1) downto 0);
	
	-- RAM controller
	component ram_ctrl
		port(
			clk				: in	std_logic; -- Driven by master clock
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			
			-- Address lines
			ctrl_addr		: in	std_logic_vector(17 downto 0); -- Send this to RAM chip
			RAM_addr			: out	std_logic_vector(17 downto 0); -- Goes to external ram chip
			
			-- Data lines
			ctrl_data_in	: in	std_logic_vector(15 downto 0); -- Send this to RAM chip if in write mode
			ctrl_data_out	: out	std_logic_vector(15 downto 0); -- Send data back to effect stage
			RAM_data_in		: in	std_logic_vector(15 downto 0); -- Input from RAM chip
			RAM_data_out	: out	std_logic_vector(15 downto 0); -- Output to RAM chip
			
			-- State machine selection signals
			ctrl_rw			: in	std_logic; -- High = read, low = write
			ctrl_enable		: in	std_logic; -- High = start
			
			-- Ram control signals
			RAM_oe			: out	std_logic; -- Output enable, active low
			RAM_ce			: out	std_logic; -- Chip enable, active low
			RAM_ub			: out	std_logic; -- Byte enable, active low
			RAM_lb			: out std_logic;
			RAM_we			: out	std_logic; -- Write enable, active low
			
			-- Return control signal
			ctrl_done		: out	std_logic
		);
	end component;
	
	-- RAM buffer signals
	signal ram_data_out_buf	:	std_logic_vector(15 downto 0); -- Buffered output register
	signal mem_we_buf			:	std_logic;
	
	-- RAM controller internal signals
	signal ctrl_addr	:	std_logic_vector(17 downto 0);
	signal ctrl_data_in, ctrl_data_out	:	std_logic_vector(15 downto 0);
	signal ctrl_rw, ctrl_enable, ctrl_done	:	std_logic;
	
	-- RAM controller multiplexer signals
	type ram_mux_addr_type is array (2 downto 0) of std_logic_vector(17 downto 0);
	signal ram_mux_addr	:	ram_mux_addr_type;
	type ram_mux_data_type is array (2 downto 0) of std_logic_vector(15 downto 0);
	signal ram_mux_data	:	ram_mux_data_type;
	signal ram_mux_rw, ram_mux_enable, ram_mux_selector	:	std_logic_vector(2 downto 0);
	
	component SPI_Sync
		port (
			audio_cs	:	in		std_logic;	-- Unsychronized signal
			ctrl_cs	:	in		std_logic;	-- Unsychronized signal
			clk		:	in		std_logic;	-- Synchronize to this clock signal
			sync		:	out	std_logic	-- Syncronized signal
		);
	end component;
	
	-- Clock divider signal for testing purposes
	signal clk_divided	:	std_logic	:=	'0';
	
	component OneTickDelay
		port (
			async	:	in		std_logic;	-- Unsychronized signal
			clk	:	in		std_logic;	-- Synchronize to this clock signal
			sync	:	out	std_logic	-- Syncronized signal
		);
	end component;
	
begin
	
	-- Divided clock
	process (clk)
		variable count	:	integer range 0 to 9 := 0;
	begin
		if (rising_edge(clk)) then
			if (count = 9) then
				count := 0;
				clk_divided <= not clk_divided;
			else
				count := count + 1;
			end if;
		end if;
	end process;
	
	sync_audio	:	OneTickDelay
		port map (
			async => audio_cs_n,	-- Unsychronized signal
			clk => clk,	-- Synchronize to this clock signal
			sync => load_stage(0)	-- Syncronized signal
		);
	
	sync_module	:	SPI_Sync
		port map (
			audio_cs => audio_cs_n,
			ctrl_cs => ctrl_cs_n,
			clk => clk,
			sync => open
		);
	
	desmux	:	spi_mux
		port map
		(
			clk => clk,
			reset => reset,
			ready => ctrl_spi_done,
			spi_data => ctrl_rx_reg,
			outputReg1 => mux_out(0),
			outputReg2 => mux_out(1),
			outputReg3 => mux_out(2),
			outputReg4 => mux_out(3),
			outputReg5 => mux_out(4),
			outputReg6 => mux_out(5)
		);
	
	effect_input(0) <= audio_rx_sign;
	E1	:	LinearGain
		port map
		(
			clk => clk,
			input_audio => effect_input(0),
			clear => reset,
			reset => load_stage(0),
			disable => enables(3),
			load_in => load_stage(0),
			effectReg1 => mux_sync(0),
			effectReg2 => mux_sync(1),
			load_out => audio_ready,
			output_audio => effect_input(1),
			RAM_address => ram_mux_addr(0),
			--RAM_address => ctrl_addr,
			RAM_data_out => ram_mux_data(0),
			--RAM_data_out => ctrl_data_in,
			RAM_data_in => ctrl_data_out,
			RAM_rw => ram_mux_rw(0),
			--RAM_rw => ctrl_rw,
			RAM_enable => ram_mux_enable(0),
			--RAM_enable => ctrl_enable,
			RAM_done => ctrl_done
		);
	
	E2	:	LinearGain
		port map
		(
			clk => clk,
			input_audio => effect_input(1),
			clear => reset,
			reset => load_stage(0),
			disable => enables(2),
			load_in => load_stage(1),
			effectReg1 => mux_sync(2),
			effectReg2 => mux_sync(3),
			load_out => load_stage(1),
			output_audio => effect_input(2),
			RAM_address => ram_mux_addr(1),
			--RAM_address => ctrl_addr,
			RAM_data_out => ram_mux_data(1),
			--RAM_data_out => ctrl_data_in,
			RAM_data_in => ctrl_data_out,
			RAM_rw => ram_mux_rw(1),
			--RAM_rw => ctrl_rw,
			RAM_enable => ram_mux_enable(1),
			--RAM_enable => ctrl_enable,
			RAM_done => ctrl_done
		);
	
	E3	:	LinearGain
		port map
		(
			clk => clk,
			input_audio => effect_input(2),
			clear => reset,
			reset => load_stage(0),
			disable => enables(1),
			load_in => load_stage(2),
			effectReg1 => mux_sync(4),
			effectReg2 => mux_sync(5),
			load_out => load_stage(2),
			output_audio => audio_tx_sign,
			RAM_address => ram_mux_addr(2),
			--RAM_address => ctrl_addr,
			RAM_data_out => ram_mux_data(2),
			--RAM_data_out => ctrl_data_in,
			RAM_data_in => ctrl_data_out,
			RAM_rw => ram_mux_rw(2),
			--RAM_rw => ctrl_rw,
			RAM_enable => ram_mux_enable(2),
			--RAM_enable => ctrl_enable,
			RAM_done => ctrl_done
		);
	
	ram_controller	:	ram_ctrl
		port map
		(
			clk => clk_divided,
			reset => reset,
			ctrl_addr => ctrl_addr,
			RAM_addr => mem_address,
			ctrl_data_in => ctrl_data_in,
			ctrl_data_out => ctrl_data_out,
			ram_data_in => mem_data,
			ram_data_out => ram_data_out_buf,
			ctrl_rw => ctrl_rw,
			ctrl_enable => ctrl_enable,
			RAM_oe => mem_oe,
			RAM_ce => mem_ce,
			RAM_ub => mem_hb,
			RAM_lb => mem_lb,
			RAM_we => mem_we_buf,
			ctrl_done => ctrl_done
		);
	
	audio_spi :	spi_slave
		port map
		(
			cs_n => audio_cs_n,
			mosi => audio_mosi,
			miso => audio_miso,
			reset_n => reset,
			sclk => audio_sclk,
			tx_reg => audio_tx_reg,
			rx_reg => audio_rx_reg,
			done => audio_spi_done,
			debug_out => open
		);
	
	ctrl_spi : spi_slave
		port map
		(
			cs_n => ctrl_cs_n,
			mosi => ctrl_mosi,
			miso => ctrl_miso,
			reset_n => reset,
			sclk => ctrl_sclk,
			tx_reg => ctrl_tx_reg,
			rx_reg => ctrl_rx_reg,
			done => ctrl_spi_done,
			debug_out => open
		);
	
	audio_tx_reg(10 downto 0) <= std_logic_vector(audio_tx_sign(10 downto 0));
	audio_tx_reg(11) <= std_logic(not audio_tx_sign(11));
	audio_tx_reg(15 downto 12) <= "0000";
	
	audio_rx_sign(11) <= not audio_rx_reg(11);
	audio_rx_sign(10 downto 0) <= signed(audio_rx_reg(10 downto 0));
	
	-- Synchronize loading the effect registers
	process (load_stage(0))
	begin
		if (load_stage(0) = '1') then
			for i in 5 downto 0 loop
				mux_sync(i) <= mux_out(i) xor "111111111111";
			end loop;
		end if;
	end process;
	
	-- Multiplex the signals to/from the RAM controller
	ram_mux_selector <= load_stage;
	
	process (ram_mux_selector, ram_mux_addr, ram_mux_data, ram_mux_enable, ram_mux_rw)
	begin
		case ram_mux_selector is
			when "100" =>
				ctrl_addr <= ram_mux_addr(0);
				ctrl_data_in <= ram_mux_data(0);
				ctrl_enable <= ram_mux_enable(0);
				ctrl_rw <= ram_mux_rw(0);
			when "-10" =>
				ctrl_addr <= ram_mux_addr(1);
				ctrl_data_in <= ram_mux_data(1);
				ctrl_enable <= ram_mux_enable(1);
				ctrl_rw <= ram_mux_rw(1);
			when "--1" =>
				ctrl_addr <= ram_mux_addr(2);
				ctrl_data_in <= ram_mux_data(2);
				ctrl_enable <= ram_mux_enable(2);
				ctrl_rw <= ram_mux_rw(2);
			when others =>
				ctrl_addr <= (others => '1');
				ctrl_data_in <= (others => '1');
				ctrl_enable <= '0';
				ctrl_rw <= '1';
		end case;
	end process;
	
	ram_bi_loop: for i in 15 downto 0 generate
		mem_data(i) <= 'Z' when mem_we_buf = '1' else  ram_data_out_buf(i);
	end generate;
	
	mem_we <= mem_we_buf;
	
	-- LEDS <= (others => '1');
	LEDS(0) <= enables(3);
	LEDS(1) <= enables(2);
	LEDS(2) <= enables(1);
	LEDS(3) <= enables(0);

end rtl;
