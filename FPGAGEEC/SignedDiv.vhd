-- Signed divider, uses unsigned quick divider with conditioned input
-- for 12-bit 2's-compliment division.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SignDiv is
	port (
		val_in		:	in		signed(11 downto 0);
		div			:	in		std_logic_vector(7 downto 0);
		val_out		:	out	signed(11 downto 0)
	);
end entity;

architecture rtl of SignDiv is
	component QuickDiv
		PORT(
			upper		:	in		unsigned(17 downto 0);
			lower		:	in		unsigned(17 downto 0);
			div		:	in		std_logic_vector(7 downto 0);
			val_out	:	out	unsigned(17 downto 0)
		);
	end component;
	
	signal upper, lower	:	unsigned(17 downto 0);
	signal val_out_inter	:	unsigned(17 downto 0);
	signal din_internal	:	std_logic_vector(7 downto 0);
	
begin
	
	Div_Module	:	QuickDiv
		port map(
			upper => upper,
			lower => lower,
			div => div,
			val_out => val_out_inter
		);
	
	-- Set up the signals
	upper(17 downto 11) <= (others => '0');
	lower(17 downto 11) <= (others => '0');
	
	process (val_in)
	begin
		if (val_in(11) = '1') then -- Negative
			upper(10 downto 0) <= unsigned(val_in(10 downto 0));
			lower(10 downto 0) <= (others => '1'); -- Lowest negative magnitude is ones
		else -- Positive
			upper(10 downto 0) <= unsigned(val_in(10 downto 0));
			lower(10 downto 0) <= (others => '0'); -- Lowest positive value is all zeros
		end if;
	end process;
	
	-- Output
	val_out(11) <= val_in(11); -- Signed bit doesn't change
	val_out(10 downto 0) <= signed(val_out_inter(10 downto 0));
end rtl;
