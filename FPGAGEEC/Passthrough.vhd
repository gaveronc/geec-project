-- Dummy Stage
-- A placeholder for effects that actually do things

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity Passthrough is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		MUL_WIDTH		:	natural	:= 12
	);
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of Passthrough is
begin
	-- Do literally nothing to the signals
	load_out <= load_in;
	output_audio <= input_audio;
	RAM_address <= (others => '1');
	RAM_data_out <= (others => '1');
	RAM_rw <= '1';
	RAM_enable <= '0';
end rtl;
