-- A parallel data module that follows a defined protocol

library ieee;
use ieee.std_logic_1164.all;

entity Transceiver is

	port(
		clk					: in	std_logic;
		reset					: in	std_logic;
		done					: in	std_logic; -- Indicates effect stages are done
		effect_data_in		: in 	std_logic_vector(11 downto 0); -- Output of final effect stage
		load_first_stage	: out	std_logic;
		effect_data_out	: out	std_logic_vector(11 downto 0); -- Output to first effect stage
		data_bus				: inout	std_logic_vector(15 downto 0) -- Holds ADC value and control data
	);

end entity;

architecture rtl of Transceiver is

	-- Build an enumerated type for the state machine
	type state_type is (idle, tx_wait, effect_wait, send);

	-- Register to hold the current state
	signal state   : state_type;

begin

	-- Logic to advance to the next state
	process (clk, reset)
	begin
		if (reset = '0') then
			state <= idle;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					if (data_bus(15) = '0') then
						state <= tx_wait;
					else
						state <= idle;
					end if;
				when tx_wait=>
					if (data_bus(15) = '1') then
						state <= effect_wait;
					else
						state <= tx_wait;
					end if;
				when effect_wait=>
					if (done = '1') then
						state <= send;
					else
						state <= effect_wait;
					end if;
				when send =>
					if (reset = '0' or data_bus(13) = '0') then
						state <= idle;
					else
						state <= send;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state)
	begin
		case state is
			when idle =>
				data_bus <= (others => '1');
				effect_data_out <= data_bus(11 downto 0);
			when tx_wait =>
				data_bus <= (others => '1');
				effect_data_out <= data_bus(11 downto 0);
			when effect_wait =>
				data_bus <= (others => '1');
				effect_data_out <= data_bus(11 downto 0);
			when send =>
				data_bus(14) <= '0';
				data_bus(11 downto 0) <= effect_data_in;
		end case;
	end process;

end rtl;
