-- This is a really simple SPI module.
-- Mode 0,0
-- Data valid on rising edge of clock.
-- Does not check for overflow/underflow.
-- Thinks it's done when cs goes high.

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY spi_slave IS
  PORT(
    -- Will connect to external SPI Master
    cs_n    : in    std_logic;
    mosi    : in    std_logic;
    miso    : out   std_logic;
    reset_n : in    std_logic;
    sclk    : in    std_logic;
    
    -- Internal signals
    tx_reg  : in    std_logic_vector(15 downto 0);
    rx_reg  : out   std_logic_vector(15 downto 0);
    done    : out   std_logic;
	 
	 -- Debug Signal
	 debug_out : out std_logic_vector(15 downto 0)
  );
END spi_slave;

ARCHITECTURE logic OF spi_slave IS
	type state_type is (s_idle, s_active);
   signal state    : state_type := s_idle;
   signal next_state : state_type := state;
	signal count	: integer range 0 to 15 := 15;
   signal tx_int	: std_logic_vector(15 downto 0) := (others => '0');
   signal rx_int	: std_logic_vector(15 downto 0) := (others => '0');
    
BEGIN
	
--	process (clk)
--	begin
--		if (rising_edge(clk)) then
--			case state is
--				when s_idle =>
--					if (cs_n = '0') then
--						state_next <= s_active;
--					end if;
--				when s_active =>
--					if (count = 0) then
--						state_next <= s_idle;
--					end if;
--		end if;
--	end process;
--	
--	-- Look-ahead buffer
--	process (next_state)
--	begin
--		
--	end process;
	
    -- Shifts internal TX register on falling edge of sclk.
    TX_LOAD_PROC: process (sclk, tx_reg, reset_n, cs_n, tx_int)
    begin
		  tx_int <= tx_int;
        if (reset_n = '0') then
            tx_int <= (others => '0');
        elsif (cs_n = '1') then
            -- Load data in to tx register
            tx_int <= tx_reg;
        elsif (falling_edge(sclk)) then
            -- Shift internal tx register
            tx_int(15 downto 1) <= tx_int(14 downto 0);
            tx_int(0) <= '0';
        end if;
    end process;
    miso <= tx_int(15); -- Always put MSB on output
	 
    -- Shifts internal RX register on rising edge of sclk
    RX_PROC: process (cs_n, sclk, reset_n)
    begin
        if (reset_n = '0') then
            rx_int <= (others => '0');
        elsif (cs_n = '0' and rising_edge(sclk)) then -- Shift rx_int register
            rx_int(15 downto 1) <= rx_int(14 downto 0);
            rx_int(0) <= mosi;
        end if;
    end process;
    
    -- When cs_n goes high, latch the RX shift register's data 
    RXOUT_PROC: process (cs_n, reset_n)
    begin
		  if (reset_n = '0') then
				rx_reg <= (others => '0');
        elsif (rising_edge(cs_n)) then
            rx_reg <= rx_int;
        end if;
    end process;
    
	 debug_out <= tx_int;
    done <= cs_n;

END logic;
