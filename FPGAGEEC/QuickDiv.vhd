library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity QuickDiv is
	port (
		upper		:	in		unsigned(17 downto 0);
		lower		:	in		unsigned(17 downto 0);
		div		:	in		std_logic_vector(7 downto 0);
		val_out	:	out	unsigned(17 downto 0)
	);
end entity;

architecture rtl of QuickDiv is
begin
	-- Binary search algorithm
	process (upper, lower, div)
		variable	upper_internal, lower_internal	:	unsigned(17 downto 0);
	begin
		upper_internal := upper;
		lower_internal := lower;
		for i in 7 downto 0 loop
			if (div(i) = '1') then
				-- Move lower bound up
				lower_internal := ((upper_internal + lower_internal) srl 1);
			else
				-- Move upper bound down
				upper_internal := ((upper_internal + lower_internal) srl 1);
			end if;
			val_out <= ((lower_internal + upper_internal) srl 1);
		end loop;
	end process;
end rtl;