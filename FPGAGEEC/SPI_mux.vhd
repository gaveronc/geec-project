-- Deserialization mux
-- Writes to registers in the various effect blocks

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY spi_mux IS
	generic (
		EFFECT_WIDTH	:	natural := 12;
		SPI_WIDTH		:	natural := 16
	);
	
	PORT(
		clk			: in std_logic;
		reset			: in std_logic; -- Resets all registers
		ready 		: in  std_logic; -- Indicates spi_data is ready
		spi_data		: in	std_logic_vector((SPI_WIDTH - 1) downto 0); -- data from spi module
		outputReg1	: out	std_logic_vector((EFFECT_WIDTH - 1) downto 0); -- Goes to effect data register
		outputReg2	: out std_logic_vector((EFFECT_WIDTH - 1) downto 0);
		outputReg3 : out std_logic_vector((EFFECT_WIDTH - 1) downto 0);
		outputReg4 : out std_logic_vector((EFFECT_WIDTH - 1) downto 0);
		outputReg5 : out std_logic_vector((EFFECT_WIDTH - 1) downto 0);
		outputReg6 : out std_logic_vector((EFFECT_WIDTH - 1) downto 0)
	);
END spi_mux;

ARCHITECTURE rtl OF spi_mux IS
BEGIN
	process (ready)
	  -- Store old values
	  variable regBuf1, regBuf2, regBuf3, regBuf4, regBuf5, regBuf6  : std_logic_vector((EFFECT_WIDTH - 1) downto 0) := (others => '0');
	begin -- Latch data depending on what's sitting on the input
		if (ready = '1') then
		  case spi_data(15 downto 12) is -- Load data into register
		    when "0001" => regBuf1 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when "0010" => regBuf2 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when "0011" => regBuf3 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when "0100" => regBuf4 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when "0101" => regBuf5 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when "0110" => regBuf6 := spi_data((EFFECT_WIDTH - 1) downto 0);
		    when others => 
		  end case;
		  -- Update registers
			outputReg1 <= regBuf1;
			outputReg2 <= regBuf2;
			outputReg3 <= regBuf3;
			outputReg4 <= regBuf4;
			outputReg5 <= regBuf5;
			outputReg6 <= regBuf6;
		end if;
	end process;

END rtl;
