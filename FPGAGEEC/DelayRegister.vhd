-- Quartus II VHDL Template
-- Basic Shift Register with Multiple Taps

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DelayRegister is

	generic
	(
		DATA_WIDTH : natural := 12;
		NUM_STAGES : natural := 1024 -- 2 ^ 10
	);

	port 
	(
		clk			: in std_logic;
		enable		: in std_logic;
		sr_in			: in std_logic_vector((DATA_WIDTH-1) downto 0);
		sr_select	: in unsigned(10 downto 0);
		sr_out		: out std_logic_vector((DATA_WIDTH-1) downto 0)
	);

end entity;

architecture rtl of DelayRegister is

	-- Build a 2-D array type for the shift register
	subtype sr_width is std_logic_vector((DATA_WIDTH-1) downto 0);
	type sr_length is array ((NUM_STAGES-1) downto 0) of sr_width;

	-- Declare the shift register signal
	signal sr: sr_length;

begin

	process (clk)
	begin
		if (rising_edge(clk)) then
			if (enable = '1') then
				for i in (NUM_STAGES-1) downto 1 loop
					-- Shift data by one stage; data from last stage is lost
					sr(i) <= sr(i-1);
				end loop;
				sr(0) <= sr_in;
			end if;
		end if;
	end process;

	-- Put final register on output
	sr_out <= sr(to_integer(sr_select));

end rtl;
