-- Package declaration

library ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package interface is
	type mul_array is array (2 downto 0) of signed (11 downto 0);
	type sr_out is array (2**5-1 downto 0) of signed (11 downto 0);
	type addr_mux is array (0 downto 0) of std_logic_vector (17 downto 0);
	type data_mux is array (0 downto 0) of std_logic_vector (15 downto 0);
end package interface;
