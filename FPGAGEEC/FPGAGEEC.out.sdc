## Generated SDC file "FPGAGEEC.out.sdc"

## Copyright (C) 1991-2013 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

## DATE    "Sat Feb 04 15:59:10 2017"

##
## DEVICE  "EP2C5T144C6"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {audio_sclk} -period 83.333 -waveform { 0.000 41.666 } [get_ports {audio_sclk}]
create_clock -name {audio_cs_n} -period 22727.300 -waveform { 0.000 21394.000 } [get_ports {audio_cs_n}]
create_clock -name {clk} -period 20.000 -waveform { 0.000 10.000 } [get_ports {clk}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************



#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************

set_false_path  -from  [get_clocks {clk}]  -to  [get_clocks {audio_cs_n}]
set_false_path  -from  [get_clocks {audio_cs_n}]  -to  [get_clocks {audio_sclk}]
set_false_path  -from  [get_clocks {audio_cs_n}]  -to  [get_clocks {clk}]
set_false_path  -from  [get_clocks {clk}]  -to  [get_clocks {audio_sclk}]
set_false_path  -from  [get_clocks {audio_sclk}]  -to  [get_clocks {audio_cs_n}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

