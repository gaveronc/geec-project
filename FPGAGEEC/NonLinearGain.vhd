-- Non-linear Gain
-- Implements a 2nd-order non-linear gain transfer function
-- by way of linear interpolation
-- 
-- EffectReg1 = Region 1 Max
-- EffectReg2 = Region 1 boundary
-- 
-- Transfer function looks as such:
-- 
--          |--------| ~ Region 2 boundary
--     _ |   ________/
--     | |  /
-- Max~| | /
--     | |/
--     - -------------
--       |--| ~ Region 1 boundary
-- 
-- The region 1 boundary determines over what range of input
-- will the output be in the range of the region 1 maximum.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity NonLinearGain is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		MUL_WIDTH		:	natural	:= 12
	);
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of NonLinearGain is

	-- Build an enumerated type for the state machine
	type state_type is (idle, onetick, done);

	-- Register to hold the current state
	signal state   : state_type;
	
	component QuickDiv
		port (
			upper		:	in		unsigned(17 downto 0);
			lower		:	in		unsigned(17 downto 0);
			div		:	in		std_logic_vector(7 downto 0);
			val_out	:	out	unsigned(17 downto 0)
		);
	end component;
	
	signal up_div, low_div, div_val	:	unsigned (17 downto 0);
	signal div_inp, cond_inp	:	std_logic_vector(7 downto 0);
	
begin
	
	div1	:	QuickDiv
		port map(
			upper => up_div,
			lower => low_div,
			div => div_inp,
			val_out => div_val
		);
	
	-- Set up (disable) RAM signals
	RAM_enable <= '0';
	RAM_address <= (others => '0');
	RAM_data_out <= (others => '0');
	RAM_rw <= '1';
	
	-- Condition the input signal
	process (input_audio, EffectReg2)
		variable temp	:	std_logic_vector(7 downto 0); -- Temp variable for input audio
	begin
		if (input_audio(11) = '1') then -- Invert negative value
			temp := std_logic_vector(input_audio(10 downto 3)) xor "11111111";
		else
			temp := std_logic_vector(input_audio(10 downto 3));
		end if;
		
		if (unsigned(temp) < unsigned(EffectReg2(11 downto 4))) then
			div_inp <= temp;
		else
			div_inp <= std_logic_vector((unsigned(temp) - unsigned(EffectReg2(11 downto 4))));
		end if;
	end process;
	
	-- Set up divider signals
	process(EffectReg1, EffectReg2, input_audio)
	begin
		if (unsigned(EffectReg1) < unsigned(EffectReg2)) then -- Gain less than non-linear region = linear
			up_div(10 downto 0) <= "11111111111";
			low_div(10 downto 0) <= (others => '0');
		elsif (unsigned(input_audio(10 downto 0)) < unsigned(EffectReg2(11 downto 1))) then -- Linear region
			up_div(10 downto 0) <= unsigned(EffectReg1(11 downto 1));
			low_div(10 downto 0) <= (others => '0');
		else -- Non-linear region
			up_div(10 downto 0) <= "11111111111";
			low_div(10 downto 0) <= unsigned(EffectReg1(11 downto 1));
		end if;
	end process;

	-- Logic to advance to the next state
	process (clk, reset, clear)
	begin
		if (clear = '0') then
			state <= idle;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					if (load_in = '1') then
						state <= onetick;
					else
						state <= idle;
					end if;
				when onetick => -- Wait a tick
					state <= done;
				when done=>
					if (reset = '0') then
						state <= idle; -- Go back to idle state
					else
						state <= done;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state, div_val, disable, input_audio)
	begin
		output_audio <= input_audio;
		case state is
			when idle =>
				load_out <= '0';
			when onetick =>
				if (input_audio(11) = '1') then -- Negative value
					output_audio(10 downto 0) <= signed(div_val(10 downto 0) xor "11111111111");
				else -- Positive value
					output_audio(10 downto 0) <= signed(div_val(10 downto 0));
				end if;
				load_out <= '0';
			when done =>
				if (input_audio(11) = '1') then -- Negative value
					output_audio(10 downto 0) <= signed(div_val(10 downto 0) xor "11111111111");
				else -- Positive value
					output_audio(10 downto 0) <= signed(div_val(10 downto 0));
				end if;
				load_out <= '1';
		end case;
	end process;

end rtl;
