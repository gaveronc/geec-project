-- Shared resources block

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all; -- Brings in the multiplier array type

entity shared_resources is
	
	generic
	(
		MUL_WIDTH 		: natural := 12;
		MUL_IN_WIDTH	: natural := 2;
		EFFECT_NUM		: natural := 3
	);

	port 
	(
		-- Multiplier 0
		shared_mul_in_0_a	   : in mul_array;
		shared_mul_in_0_b		: in mul_array;
		result_0					: out signed ((2*MUL_WIDTH-1) downto 0);
		
		-- Multiplier 1
		shared_mul_in_1_a	   : in mul_array;
		shared_mul_in_1_b		: in mul_array;
		result_1					: out signed ((2*MUL_WIDTH-1) downto 0);
		
		-- Control signals
		requests_in				: in std_logic_vector((EFFECT_NUM-1) downto 0);
		requests_out			: out std_logic_vector((EFFECT_NUM-1) downto 0);
		
		clk						: in std_logic
	);

end shared_resources;

architecture rtl of shared_resources is
	
	component signed_multiply
		port 
		(
			a	   : in signed ((MUL_WIDTH-1) downto 0);
			b	   : in signed ((MUL_WIDTH-1) downto 0);
			result  : out signed ((2*MUL_WIDTH-1) downto 0)
		);
	end component;
	
	-- Build an enumerated type for the state machine
	type state_type is (ready_state, active_state);

	-- Register to hold the current state
	signal state   : state_type;
	
	-- Multiplier signals
	signal mul_0_a, mul_0_b, mul_1_a, mul_1_b	: signed ((MUL_WIDTH-1) downto 0);
	
	-- Service signals
	signal selection	: integer range (EFFECT_NUM/2) downto 0;
begin
	
	mul1: signed_multiply
		port map (
			a => mul_0_a,
			b => mul_0_b,
			result => result_0
		);
	
	mul2: signed_multiply
		port map (
			a => mul_1_a,
			b => mul_1_b,
			result => result_1
		);
	
	process (clk)
		variable choice	:	integer range (EFFECT_NUM/2) downto 0;
	begin
		if(rising_edge(clk)) then
			case state is
				when ready_state =>
					state <= ready_state;
					choice := 0;
					for i in (EFFECT_NUM-1) downto 0 loop
						if (requests_in(i) = '1') then
							choice := i;
							state <= active_state;
						end if;
					end loop;
				when active_state =>
					if (requests_in(choice) = '0') then
						state <= ready_state;
					else
						state <= active_state;
					end if;
			end case;
			selection <= choice;
		end if;
	end process;
	
	-- Drive the mux with the selection signal
	mul_0_a <= shared_mul_in_0_a(selection);
	mul_0_b <= shared_mul_in_0_b(selection);
	mul_1_a <= shared_mul_in_1_a(selection);
	mul_1_b <= shared_mul_in_1_b(selection);
	
	-- Output register, drives request service lines
	process (state, selection)
	begin
		requests_out <= (others => '0');
		case state is
			when ready_state =>
				requests_out <= (others => '0');
			when active_state =>
				requests_out <= (others => '0');
				requests_out(selection) <= '1';
		end case;
	end process;
	
end rtl;

