library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_ctrl is
	
	port(
		clk				: in	std_logic; -- Driven by master clock
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		
		-- Address lines
		ctrl_addr		: in	std_logic_vector(17 downto 0); -- Send this to RAM chip
		RAM_addr			: out	std_logic_vector(17 downto 0); -- Goes to external ram chip
		
		-- Data lines
		ctrl_data_in	: in	std_logic_vector(15 downto 0); -- Send this to RAM chip if in write mode
		ctrl_data_out	: out	std_logic_vector(15 downto 0); -- Send data back to effect stage
		RAM_data_in		: in	std_logic_vector(15 downto 0); -- Input from RAM chip
		RAM_data_out	: out	std_logic_vector(15 downto 0); -- Output to RAM chip
		
		-- State machine selection signals
		ctrl_rw			: in	std_logic; -- High = read, low = write
		ctrl_enable		: in	std_logic; -- High = start
		
		-- Ram control signals
		RAM_oe			: out	std_logic; -- Output enable, active low
		RAM_ce			: out	std_logic; -- Chip enable, active low
		RAM_ub			: out	std_logic; -- Byte enable, active low
		RAM_lb			: out std_logic;
		RAM_we			: out	std_logic; -- Write enable, active low
		
		-- Return control signal
		ctrl_done		: out	std_logic
	);

end entity;

architecture rtl of ram_ctrl is
	-- RAM state machine
	type ram_state_type is (idle, w0, w1, r0, r1, r2);
	signal ram_state   : ram_state_type;
begin
	-- Parallel memory interface
	-- State transition
	process (clk)
	begin
		if (rising_edge(clk)) then
			case ram_state is
				when idle=>
					if (ctrl_enable = '1' and ctrl_rw = '1') then
						ram_state <= r0;
					elsif (ctrl_enable = '1') then
						ram_state <= w0;
					else
						ram_state <= idle;
					end if;
				when r0=>
					ram_state <= r1;
				when r1=>
					ram_state <= r2;
				when r2=>
					if (ctrl_enable = '0') then
						ram_state <= idle;
					else
						ram_state <= r2;
					end if;
				when w0=>
					ram_state <= w1;
				when w1=>
					if (ctrl_enable = '0') then
						ram_state <= idle;
					else
						ram_state <= w1;
					end if;
			end case;
		end if;
	end process;
	
	-- RAM state machine output
	process (ram_state)
	begin
		ram_data_out <= (others => '1');
		ram_oe <= '1';
		ram_ce <= '1';
		ram_ub <= '1';
		ram_lb <= '1';
		ctrl_done <= '0';
		ram_we <= '1';
		case ram_state is
			when idle =>
				-- Uses defaults
			when r0 =>
				ram_ce <= '0';
			when r1 =>
				ram_ce <= '0';
				ram_oe <= '0';
				ram_lb <= '0';
				ram_ub <= '0';
				ctrl_data_out <= ram_data_in;
			when r2 =>
				ram_ce <= '0';
				ram_oe <= '0';
				ram_lb <= '0';
				ram_ub <= '0';
				ctrl_data_out <= ram_data_in;
				ctrl_done <= '1';
			when w0 =>
				ram_ce <= '0';
				ram_lb <= '0';
				ram_ub <= '0';
				ram_we <= '0';
				ram_data_out <= ctrl_data_in;
			when w1 =>
				ctrl_done <= '1';
		end case;
	end process;
	
	ram_addr <= ctrl_addr;
end rtl;
