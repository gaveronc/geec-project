-- Delay effect
-- Essentially a feed-forward system that causes a single "echo" to happen:
-- 
-- Input -----------(+)-----> Output
--         |         ^
--         |         |
--         ->(a*z^-n)-
-- 
-- Where "n" is the number of samples delayed and "a" is the attenuation.
-- 
-- The effect is currently configured to store up to 1 second of audio at
-- a sample rate of 44kHz. It is configured like a ring buffer, writing to
-- an external RAM chip, reading one of the previous addresses as per the
-- value specified in EffectReg1, adding that value to the input, and
-- putting it onto the output. EffectReg2 specifies the amount
-- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity DelayEffect is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		OFFSET_MAX		:	natural	:= 44000
	);
	
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of DelayEffect is

	-- Build an enumerated type for the state machine
	type state_type is (idle, R_wait, R_clear, W_wait, done);

	-- Register to hold the current state
	signal state   : state_type;
	
	-- Effect control values
	signal upper_range, lower_range, offset, tap_out, tap_upper	:	unsigned (17 downto 0);
	signal tap_loc	:	unsigned (17 downto 0) := (others => '0');
	signal addr_off	:	std_logic_vector(11 downto 0);
	
	-- Embedded divider
	component QuickDiv
		port (
			upper		:	in		unsigned(17 downto 0);
			lower		:	in		unsigned(17 downto 0);
			div		:	in		std_logic_vector(7 downto 0);
			val_out	:	out	unsigned(17 downto 0)
		);
	end component;
	
begin
	
	-- Find the feed-forward tap location based on the latched offset value
	div1: QuickDiv port map (
			upper => upper_range,
			lower => lower_range,
			div => addr_off(11 downto 4),
			val_out => offset
		);
	
	upper_range <= to_unsigned(44000, 18);
	lower_range <= to_unsigned(0, 18);
	
	-- Attenuates the feed-forward value
	div2: QuickDiv port map (
			upper => tap_upper,
			lower => lower_range,
			div => effectReg2(11 downto 4),
			val_out => tap_out
		);
	
	tap_upper(17 downto 16) <= "00";
	
	
	-- Logic to advance to the next state
	process (clk, reset, clear)
		variable tap_update	:	unsigned(17 downto 0);
	begin
		if (clear = '0') then
			state <= idle;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					if (load_in = '1') then
						state <= R_wait;
					else
						state <= idle;
					end if;
				when R_wait=> -- Read tap, wait for return
					if (RAM_done = '1') then
						state <= R_clear;
					else
						state <= R_wait;
					end if;
				when R_clear=>
					state <= W_wait;
				when W_wait=>
					if (RAM_done = '1') then
						state <= done;
						tap_update := tap_loc + 1;
						if (tap_update > OFFSET_MAX) then
							tap_update := to_unsigned(0, 18);
						end if;
						tap_loc <= tap_update;
					else
						state <= W_wait;
					end if;
				when done=>
					if (reset = '0') then
						state <= idle; -- Go back to idle state
					else
						state <= done;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state, input_audio)
	begin
		output_audio <= input_audio;
		load_out <= '0';
		RAM_enable <= '0';
		RAM_rw <= '1';
		if (tap_loc < offset) then -- 
			RAM_address <= std_logic_vector(upper_range + tap_loc - offset);
		else
			RAM_address <= std_logic_vector(tap_loc - offset);
		end if;
		case state is
			when idle =>
			when R_wait=>
				RAM_rw <= '1';
				RAM_enable <= '1';
			when R_clear=>
				RAM_enable <= '0';
			when W_wait=>
				RAM_address <= std_logic_vector(tap_loc);
				RAM_rw <= '0';
				RAM_enable <= '1';
			when done =>
				if(disable = '0') then
					output_audio <= input_audio + signed(tap_out(11 downto 0));
				end if;
				load_out <= '1';
		end case;
	end process;
	
	-- May as well load the desired data onto the output
	RAM_data_out(11 downto 0) <= std_logic_vector(input_audio);
	RAM_data_out(15 downto 12) <= (others => '0');
	
	-- Update tap location when disable is toggled
	process (disable)
	begin
		addr_off <= effectReg1;
	end process;
	
	-- Latch the ram data
	process (RAM_done)
	begin
		if (RAM_done = '1' and state = R_clear) then
			tap_upper(15 downto 0) <= unsigned(ram_data_in);
		end if;
	end process;

end rtl;
