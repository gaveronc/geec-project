-- Linear Gain
-- Applies linear gain to the input audio signal, then saturates (clips) if
-- the amplitude is above the threshold specified in the effect register.
-- 
-- EffectReg1 = Gain (max 2, min 0)
-- EffectReg2 = Saturation value

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity LinearGain is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		MUL_WIDTH		:	natural	:= 12
	);
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of LinearGain is

	-- Build an enumerated type for the state machine
	type state_type is (idle, onetick, done);

	-- Register to hold the current state
	signal state   : state_type;
	
	component SignDiv
		port (
			val_in		:	in		signed(11 downto 0);
			div			:	in		std_logic_vector(7 downto 0);
			val_out		:	out	signed(11 downto 0)
		);
	end component;
	
	signal div_val	:	signed (11 downto 0);
	signal effectReg2_inv	:	std_logic_vector(11 downto 0); -- Invert to be more intuitive to user
	
begin
	
	div1	:	SignDiv
		port map(
			val_in => input_audio,
			div => effectReg1(11 downto 4),
			val_out => div_val
		);
	
	-- Output process
	effectReg2_inv <= effectReg2 xor "111111111111";
	process(div_val, effectReg2, disable)
		variable temp_out	:	signed(11 downto 0);
	begin
		if (disable = '1') then -- Effect active
			temp_out(11) := div_val(11);
			temp_out(10 downto 1) := div_val(9 downto 0); -- Shift by 1
			temp_out(0) := '0';
			if (div_val(11) = '1' and (((unsigned(abs(temp_out))) > (unsigned(effectReg2_inv(11 downto 1)))) or (div_val(10) = '0'))) then -- Negative saturation
				temp_out(10 downto 0) := signed(effectReg2_inv(11 downto 1) xor "11111111111");
			elsif (div_val(11) = '0' and ((unsigned(temp_out(10 downto 0)) > unsigned(effectReg2_inv(11 downto 1))) or (div_val(10) = '1'))) then
				temp_out(10 downto 0) := signed(effectReg2_inv(11 downto 1));
			end if;
		else -- Effect inactive
			temp_out := input_audio;
		end if;
		output_audio <= temp_out;
	end process;
	
	-- Set up (disable) RAM signals
	RAM_enable <= '0';
	RAM_address <= (others => '0');
	RAM_data_out <= (others => '0');
	RAM_rw <= '1';

	-- Logic to advance to the next state
	process (clk, reset, clear)
	begin
		if (clear = '0') then
			state <= idle;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					if (load_in = '1') then
						state <= onetick;
					else
						state <= idle;
					end if;
				when onetick => -- Wait a tick
					state <= done;
				when done=>
					if (reset = '0') then
						state <= idle; -- Go back to idle state
					else
						state <= done;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state, div_val, disable)
	begin
		load_out <= '0';
		case state is
			when idle =>
			when onetick =>
			when done =>
				load_out <= '1';
		end case;
	end process;

end rtl;
