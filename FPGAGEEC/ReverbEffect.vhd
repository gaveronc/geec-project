-- Reverb effect
-- Essentially a simple feedback system:
-- 
-- Input -(+)-------------> Output
--         ^          |
--         |          |
--         --(a*z^-n)<-
-- 
-- Where "n" is the number of samples delayed and "a" is the attenuation.
-- 
-- This is quite similar to the delay effect, only this one uses the FPGA's
-- on-board RAM to create a ring buffer, then a quick divider to attenuate
-- the delayed value.
-- 
-- EffectReg1 = attenuation
-- EffectReg2 = delay

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity ReverbEffect is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12
	);
	
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of ReverbEffect is

	-- Build an enumerated type for the state machine
	type state_type is (idle, onetick, done);

	-- Register to hold the current state
	signal state   : state_type;
	
	-- Effect control values
	signal upper_range, lower_range, att_rev_sample	:	unsigned (17 downto 0);
	signal reverb_sample	:	std_logic_vector(11 downto 0);
	
	-- Embedded divider
	component QuickDiv
		port (
			upper		:	in		unsigned(17 downto 0);
			lower		:	in		unsigned(17 downto 0);
			div		:	in		std_logic_vector(7 downto 0);
			val_out	:	out	unsigned(17 downto 0)
		);
	end component;
	
	component DelayRegister
		port 
		(
			clk			: in std_logic;
			enable		: in std_logic;
			sr_in			: in std_logic_vector((AUDIO_WIDTH-1) downto 0);
			sr_select	: in unsigned(10 downto 0);
			sr_out		: out std_logic_vector((AUDIO_WIDTH-1) downto 0)
		);
	end component;
	
	signal sr_in, sr_out	:	std_logic_vector((AUDIO_WIDTH-1) downto 0);
	signal shift	:	std_logic;
	
begin
	
	-- Find the feed-forward tap location based on the latched offset value
	div1: QuickDiv port map (
			upper => upper_range,
			lower => lower_range,
			div => reverb_sample(10 downto 3),
			val_out => att_rev_sample
		);
	
	delay: DelayRegister port map (
			clk => shift,
			enable => '1',
			sr_in => sr_in,
			sr_select => unsigned(effectReg2),
			sr_out => sr_out
		);
	
	-- Always put input onto shift register input
	sr_in <= std_logic_vector(input_audio);
	
	-- Range signals
	lower_range <= (others => '0');
	
	-- Shift on load signal
	shift <= load_in;
	reverb_sample <= sr_out(10 downto 3);
	
	-- Set up (disable) RAM signals
	RAM_enable <= '0';
	RAM_address <= (others => '0');
	RAM_data_out <= (others => '0');
	RAM_rw <= '1';
	
	-- Logic to advance to the next state
	process (clk, reset, clear)
	begin
		if (clear = '0') then
			state <= idle;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					if (load_in = '1') then
						state <= onetick;
					else
						state <= idle;
					end if;
				when onetick =>
					state <= done;
				when done=>
					if (reset = '0') then
						state <= idle; -- Go back to idle state
					else
						state <= done;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state, input_audio, att_rev_sample)
	begin
		output_audio <= input_audio;
		load_out <= '0';
		shift <= '0';
		case state is
			when idle =>
			when onetick =>
				if(disable = '0') then
					output_audio <= input_audio + signed(att_rev_sample(11 downto 0));
				end if;
			when done =>
				if(disable = '0') then
					output_audio <= input_audio + signed(att_rev_sample(11 downto 0));
				end if;
				load_out <= '1';
		end case;
	end process;

end rtl;
