-- Feedback Test Effect Stage

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity effectFeedback is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12
	);
	
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic
	);

end entity;

architecture rtl of effectFeedback is

	-- Build an enumerated type for the state machine
	type state_type is (idle, done);

	-- Register to hold the current state
	signal state   : state_type;

begin

	RAM_enable <= '0';
	
	-- Logic to advance to the next state
	process (clk, state, load_in, reset, clear)
	begin
		if (clear = '1') then
			state <= idle;
		else
			if(rising_edge(clk)) then
				case state is
					when idle=>
						if (load_in = '1') then
							state <= done;
						else
							state <= idle;
						end if;
					when done=>
						if (reset = '0') then
							state <= idle; -- Go back to idle state
						else
							state <= done;
						end if;
				end case;
			end if;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state, input_audio, disable)
	begin
		load_out <= '0';
		output_audio <= input_audio;
		case state is
			when idle =>
			when done =>
				output_audio <= input_audio xor "111111111111";
				load_out <= '1';
		end case;
	end process;

end rtl;

