-- RAM test built using the digital effect template
-- This test cycles through data values and addresses, first writing to then reading
-- from the memory location to verify that the RAM chip is working as intended.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.interface.all;

entity RamTest is
	
	generic (
		AUDIO_WIDTH		:	natural	:= 12;
		EFFECT_WIDTH	:	natural	:= 12;
		OFFSET_MAX		:	natural	:= 44000
	);
	
	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	signed((AUDIO_WIDTH-1) downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- First effect register
		effectReg2		: in	std_logic_vector((EFFECT_WIDTH-1) downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	signed((AUDIO_WIDTH-1) downto 0); -- Leads to next effect stage
		
		-- RAM ports
		RAM_address		: out std_logic_vector(17 downto 0);
		RAM_data_out	: out std_logic_vector(15 downto 0);
		RAM_data_in		: in	std_logic_vector(15 downto 0);
		RAM_rw			: out	std_logic;
		RAM_enable		: out	std_logic;
		RAM_done			: in	std_logic;
		
		-- Debug ports (non-standard)
		debug				: out	std_logic_vector(2 downto 0)
	);

end entity;

architecture rtl of RamTest is

	-- Build an enumerated type for the state machine
	type state_type is (read_ram, read_wait, read_clear, write_ram, write_wait, write_clear);

	-- Register to hold the current state
	signal state   : state_type := read_clear;
	
	signal tap_loc		:	unsigned(17 downto 0) := (others => '0');
	signal write_val, read_val	:	unsigned(15 downto 0) := (others => '0');
	signal limit_val	:	unsigned(15 downto 0) := to_unsigned(10000, 16);
	signal limit_tap	:	unsigned(17 downto 0) := to_unsigned(12000, 18);
	
begin
	
	-- Logic to advance to the next state
	process (clk)
		variable	tap_temp	:	unsigned(17 downto 0); -- Holds next address
		variable val_temp	:	unsigned(15 downto 0); -- Holds next value
	begin
		if (rising_edge(clk)) then
			case state is
				when read_ram => -- Loads data into RAM communication module
					state <= read_wait;
				when read_wait => -- Wait for RAM operation to finish
					if (RAM_done = '1') then
						read_val <= unsigned(RAM_data_in);
						state <= read_clear;
					else
						state <= read_wait;
					end if;
				when read_clear => -- Update debug value and increment location and data value
					-- Reflect whether or not the returned value matches sent value;
					if (write_val = read_val) then
						debug(1) <= '0';
					else
						debug(1) <= '1';
					end if;
					
					-- Update Tap
					tap_temp := tap_loc + 1;
					if (tap_temp > limit_tap) then
						tap_temp := (others => '0');
					end if;
					tap_loc <= tap_temp;
					
					-- Update Value
					val_temp := write_val + 1;
					if (val_temp > limit_val) then
						val_temp := (others => '0');
					end if;
					write_val <= val_temp;
					state <= write_ram;
				when write_ram => -- Load value and address into RAM module
					state <= write_wait;
				when write_wait => -- Wait for operation to complete
					if (RAM_done = '1') then
						state <= write_clear;
					else
						state <= write_wait;
					end if;
				when write_clear => -- Reset the RAM module
					state <= read_ram;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state)
	begin
		case state is
			when read_ram =>
				debug(0) <= '0'; -- Reflects read/write state
				RAM_rw <= '1'; -- Tell RAM module to read
				RAM_enable <= '1'; -- Tell RAM module to start
			when read_wait =>
				debug(0) <= '0';
				RAM_rw <= '1';
				RAM_enable <= '1';
			when read_clear =>
				debug(0) <= '0';
				RAM_rw <= '1';
				RAM_enable <= '0'; -- Tell RAM module to stop
			when write_ram =>
				debug(0) <= '1';
				RAM_rw <= '0';
				RAM_enable <= '1';
			when write_wait =>
				debug(0) <= '1';
				RAM_rw <= '0';
				RAM_enable <= '1';
			when write_clear =>
				debug(0) <= '1';
				RAM_rw <= '0';
				RAM_enable <= '0';
		end case;
	end process;
	
	RAM_address <= std_logic_vector(tap_loc);
	RAM_data_out <= std_logic_vector(write_val);
	debug(2) <= read_val(0); -- Should look like a square wave if all is good
	
end rtl;
