-- Master testbench file.
-- Each component will be instantiated individually
-- and tested for adherence to specified behaviours.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity test is
end test;

architecture TB of test is
	
	component SignDiv
		port (
			val_in		:	in		signed(11 downto 0);
			div			:	in		std_logic_vector(7 downto 0);
			val_out		:	out	signed(11 downto 0)
		);
	end component;
	
	signal signed_val_in, signed_val_out	:	signed(11 downto 0);
	signal signed_div_in	:	std_logic_vector(7 downto 0);
	
	component effectFeedback
		 port(
			clk				: in	std_logic; -- Driven by master clock
			input_audio		: in	signed(11 downto 0); -- 12-bit audio data
			reset	 			: in	std_logic; -- Clear all signals and go to idle
			clear     		: in	std_logic; -- Clear all internal registers
			disable			: in std_logic; -- Disables the effect stage
			load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
			effectReg1		: in	std_logic_vector(11 downto 0); -- First effect register
			effectReg2		: in	std_logic_vector(11 downto 0); -- Second effect register
			load_out			: out std_logic; -- Indicator for next effect stage
			output_audio	: out	signed(11 downto 0); -- Leads to next effect stage
			
			RAM_address		: out std_logic_vector(17 downto 0);
			RAM_data_out	: out std_logic_vector(15 downto 0);
			RAM_data_in		: in	std_logic_vector(15 downto 0);
			RAM_rw			: out	std_logic;
			RAM_enable		: out	std_logic;
			RAM_done			: in	std_logic
		); end component;
    
   signal clk      		: std_logic := '0';
	signal input_audio	: signed(11 downto 0);
	signal reset			: std_logic := '1';
	signal clear  		: std_logic := '1';
	signal disable		: std_logic := '0';
	signal load_in		: std_logic := '0';
	signal effectReg1, effectReg2	: std_logic_vector(11 downto 0);
	signal load_out		: std_logic;
	signal output_audio	: signed(11 downto 0);
	signal ram_address  : std_logic_vector(17 downto 0);
	signal ram_data_out	: std_logic_vector(15 downto 0);
	signal ram_data_in		: std_logic_vector(15 downto 0);
	signal ram_rw			: std_logic;
	signal ram_enable		:	std_logic;
   signal ram_done			:	std_logic;
	
	-- Full testbench simulation
	component FPGAGEEC
		port
		(
			-- Multi-pathed input ports
			reset		: in  std_logic;
			clk		: in  std_logic;
			enables	: in	std_logic_vector(3 downto 0); -- Enable switches

			-- SPI input ports
			ctrl_mosi		: in	std_logic;
			ctrl_cs_n		: in	std_logic;
			ctrl_sclk		: in	std_logic;
			
			-- SPI output ports
			ctrl_miso	: out	std_logic;
			
			-- Audio data ports
			audio_miso	: out std_logic;
			audio_mosi	: in std_logic;
			audio_cs_n	: in std_logic;
			audio_sclk	: in std_logic;
			audio_ready	: out std_logic;
			
			-- LED config
			LEDS	: out std_logic_vector(3 downto 0);
			
			-- Parallel memory device ports
			mem_data		: inout std_logic_vector(15 downto 0);
			mem_address	: out std_logic_vector(17 downto 0);
			mem_ce		: out std_logic;
			mem_oe		: out std_logic;
			mem_we		: out std_logic;
			mem_lb		: out std_logic;
			mem_hb		: out std_logic;
			
			-- Debug pins
			debug			: out std_logic_vector(2 downto 0)
		);
	end component;
	
	-- Full design signals
	-- Intermediate SPI registers
	signal audio_rx, audio_tx	:	std_logic_vector(15 downto 0);
	signal audio_en, audio_ret	:	std_logic;
	signal ctrl_rx, ctrl_tx		:	std_logic_vector(15 downto 0);
	signal ctrl_en, ctrl_ret	:	std_logic;
	
	-- Audio SPI
	signal audio_miso, audio_mosi, audio_cs_n, audio_ready, audio_sclk	:	std_logic;
	
	-- CTRL SPI
	signal ctrl_miso, ctrl_mosi, ctrl_cs_n, ctrl_ready, ctrl_sclk	:	std_logic;
	
	-- Parallel memory signals
	signal mem_address	:	std_logic_vector(17 downto 0);
	signal mem_data, mem_data_fpga, mem_data_ram		:	std_logic_vector(15 downto 0);
	signal mem_ce, mem_oe, mem_we, mem_lb, mem_hb	:	std_logic;
	
	-- Other signals
	signal m_reset	:	std_logic;
	signal LEDs, enables	:	std_logic_vector(3 downto 0);
	signal debug_out : std_logic_vector(2 downto 0);
	
	-- Binary search/divider testbench
	component QuickDiv
		port (
			upper		:	in		unsigned(17 downto 0);
			lower		:	in		unsigned(17 downto 0);
			div		:	in		std_logic_vector(7 downto 0);
			val_out	:	out	unsigned(17 downto 0)
		);
	end component;
	
	signal upper_in, lower_in, val_out	:	unsigned(17 downto 0);
	signal div_in	:	std_logic_vector(7 downto 0);
	
	type mem_array is array (((2**18)-1) downto 0) of unsigned(15 downto 0);
	
begin

    clk <= not clk after 1 ns;
	 
	 -- Effect module standalone test
   uut1: effectFeedback PORT MAP (
			clk => clk,
			input_audio => input_audio,
			reset => reset,
			clear => clear,
			disable => disable,
			load_in => load_in,
			effectReg1 => effectReg1,
			effectReg2 => effectReg2,
			load_out => load_out,
			output_audio => output_audio,
			ram_address => ram_address,
			ram_data_out => ram_data_out,
			ram_data_in => ram_data_in,
			ram_rw => ram_rw,
			ram_enable => ram_enable,
			ram_done => ram_done
      );
		
	uut2: FPGAGEEC port map(
			reset => m_reset,
			clk => clk,
			enables => enables,
			ctrl_mosi => ctrl_mosi,
			ctrl_cs_n => ctrl_cs_n,
			ctrl_sclk => ctrl_sclk,
			ctrl_miso => ctrl_miso,
			audio_miso => audio_miso,
			audio_mosi => audio_mosi,
			audio_cs_n => audio_cs_n,
			audio_sclk => audio_sclk,
			audio_ready => audio_ready,
			LEDS => LEDs,
			mem_data => mem_data_fpga,
			mem_address => mem_address,
			mem_ce => mem_ce,
			mem_oe => mem_oe,
			mem_we => mem_we,
			mem_lb => mem_lb,
			mem_hb => mem_hb,
			debug => debug_out
		);
		
	uut3: QuickDiv port map (
			upper => upper_in,
			lower => lower_in,
			div => div_in,
			val_out => val_out
		);
	
	uut4: SignDiv port map(
			val_in => signed_val_in,
			div => signed_div_in,
			val_out => signed_val_out
		);
	
	process
	begin
		signed_val_in <= "000000001111"; -- +15
		signed_div_in <= "10000000"; -- 3.125%
		wait for 10 ns; -- 1 (000000000001) 0 [0]
		signed_val_in <= "111111110001"; -- -15
		signed_div_in <= "10000000"; -- 3.125%
		wait for 10 ns; -- -1 (111111111111) -79 [111110110001]
		signed_val_in <= "000001100100"; -- +100
		signed_div_in <= "11000000"; -- 3.9%
		wait for 10 ns; -- 10 (000000001010) 3 [000000000011]
		signed_val_in <= "111110011100"; -- -100
		signed_div_in <= "11000000"; -- 3.9%
		wait for 10 ns; -- -10 (111111110110) -177 [111101001111]
		wait;
	end process;
	
   process -- Effect test process
   begin
		-- Reset the device
		disable <= '0';
		reset <= '0';
		wait for 10 ns;
		reset <= '1';
		wait for 10 ns;
		
		-- Test effect stage
		-- Simulate register values
		effectReg1 <= "111111111111";
		effectReg2 <= "000011110000";
		wait for 10 ns;
		
		input_audio <= "101010101010";
		wait for 2 ns;
		load_in <= '1';
		-- Wait until effect stage is done
		wait until load_out = '1';
		wait for 10 ns;
		-- Tell effect stage to go back to idle
		load_in <= '0';
		wait for 2 ns;
		reset <= '0';
		wait for 2 ns;
		reset <= '1';
		wait for 10 ns;
		-- Done first test
		
		-- Try disabling effect stage
		disable <= '1';
		wait for 10 ns;
		load_in <= '1';
		wait until load_out = '1';
		wait for 10 ns;
		load_in <= '0';
		wait for 2 ns;
		reset <= '0';
		wait for 2 ns;
		reset <= '1';
		wait for 10 ns;
		-- Done second test
		
		wait; -- Done (for now)
   end process;
	
	
	process -- Full design testbench
	begin
		m_reset <= '0';
		audio_en <= '0';
		audio_tx <= (others => '0');
		ctrl_en <= '0';
		ctrl_tx <= (others => '0');
		wait for 5 ns;
		m_reset <= '1';
		wait for 5 ns;
		
		-- Load the effect registers
		ctrl_tx <= "0001000000000001"; -- 1 cycle delay
		wait for 5 ns;
		ctrl_en <= '1';
		wait until ctrl_ret = '1';
		ctrl_en <= '0';
		wait for 5 ns;
		
		-- Toggle disable switch to latch data
		enables(0) <= '1';
		wait for 5 ns;
		enables(0) <= '0';
		wait for 5 ns;
		
		ctrl_tx <= "0010111111111111"; -- No attenuation on delayed value
		wait for 5 ns;
		ctrl_en <= '1';
		wait until ctrl_ret = '1';
		ctrl_en <= '0';
		wait for 5 ns;
		
		-- First sample
		audio_tx <= "0000101010101010";
		wait for 5 ns;
		audio_en <= '1';
		wait until audio_ret = '1';
		audio_en <= '0';
		wait for 50 ns;
		
		-- Second sample
		audio_tx <= "0000111100001110";
		wait for 5 ns;
		audio_en <= '1';
		wait until audio_ret = '1';
		audio_en <= '0';
		wait for 50 ns;
		
		-- Third sample
		audio_tx <= "0000111111111111";
		wait for 5 ns;
		audio_en <= '1';
		wait until audio_ret = '1';
		audio_en <= '0';
		wait for 50 ns;
		wait;
	end process;
	
	-- Audio SPI process
	process
	begin
	  wait until audio_en = '1';
		if (audio_en = '1') then
			-- Set up initial signals
			audio_cs_n <= '1';
			audio_ret <= '0';
			audio_sclk <= '0';
			wait for 10 ns;
			audio_cs_n <= '0';
			wait for 10 ns;
			-- Start transaction
			for i in 15 downto 0 loop
				audio_mosi <= audio_tx(i);
				wait for 2.5 ns;
				audio_sclk <= '1'; -- Latch MOSI
				wait for 5 ns;
				audio_sclk <= '0'; -- Latch MISO
				audio_rx(i) <= audio_miso;
				wait for 2.5 ns;
			end loop;
			audio_cs_n <= '1';
			audio_ret <= '1';
		end if;
		wait until audio_en = '0';
	end process;
	
	-- Ctrl SPI process
	process
	begin
	  wait until ctrl_en = '1';
		if (ctrl_en = '1') then
			-- Set up initial signals
			ctrl_cs_n <= '1';
			ctrl_ret <= '0';
			ctrl_sclk <= '0';
			wait for 10 ns;
			ctrl_cs_n <= '0';
			wait for 10 ns;
			-- Start transaction
			for i in 15 downto 0 loop
				ctrl_mosi <= ctrl_tx(i);
				wait for 2.5 ns;
				ctrl_sclk <= '1'; -- Latch MOSI
				wait for 5 ns;
				ctrl_sclk <= '0'; -- Latch MISO
				ctrl_rx(i) <= ctrl_miso;
				wait for 2.5 ns;
			end loop;
			ctrl_cs_n <= '1';
			ctrl_ret <= '1';
		end if;
		wait until ctrl_en = '0';
	end process;
	
	-- Binary Search testbench
	process
	begin
		upper_in <= (others => '0');
		lower_in <= (others => '0');
		div_in <= (others => '0');
		wait for 10 ns;
		upper_in(7 downto 0) <= (others => '1');
		lower_in <= (others => '0');
		div_in <= "10000000";
		wait for 10 ns;
		div_in <= "10000011";
		wait;
	end process;
	
--	-- Memory process
--	process (mem_data, mem_address, mem_ce, mem_oe, mem_hb, mem_lb, mem_we)
--		variable	data_internal	:	mem_array;
--	begin
--		if (mem_ce = '0') then -- Enabled
--			mem_data_ram <= (others => '1');
--			if (mem_we = '0') then -- Write mode
--				if (mem_hb = '0') then
--					data_internal(to_integer(unsigned(mem_address)))(15 downto 8) := unsigned(mem_data(15 downto 8));
--				end if;
--				if (mem_lb = '0') then
--					data_internal(to_integer(unsigned(mem_address)))(7 downto 0) := unsigned(mem_data(7 downto 0));
--				end if;
--			else -- Read mode
--				if (mem_oe = '0') then
--					if (mem_hb = '0') then
--						mem_data_ram(15 downto 8) <= std_logic_vector(data_internal(to_integer(unsigned(mem_address)))(15 downto 8));
--					end if;
--					if (mem_lb = '0') then
--						mem_data_ram(7 downto 0) <= std_logic_vector(data_internal(to_integer(unsigned(mem_address)))(7 downto 0));
--					end if;
--				end if;
--			end if;
--		end if;
--	end process;
	
--	-- Simulate bidirectional communications
--	tb_ram_bi_loop: for i in 15 downto 0 generate
--		mem_data(i) <= '0' when mem_data_fpga(i) = '0' or mem_data_ram(i) = '0' else 'H';
--	end generate;

	mem_data_fpga <= "LLLLLLLLLLLLLLHL";
	
end TB;

