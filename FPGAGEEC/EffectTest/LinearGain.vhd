-- Linear Gain

library ieee;
use ieee.std_logic_1164.all;

entity LinearGain is

	port(
		clk				: in	std_logic; -- Driven by master clock
		input_audio		: in	std_logic_vector(11 downto 0); -- 12-bit audio data
		reset	 			: in	std_logic; -- Clear all signals and go to idle
		clear     		: in	std_logic; -- Same as reset, but also clear internal vairables
		disable			: in	std_logic; -- Disables the effect stage
		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
		effectReg1		: in	std_logic_vector(11 downto 0); -- First effect register
		effectReg2		: in	std_logic_vector(11 downto 0); -- Second effect register
		load_out			: out std_logic; -- Indicator for next effect stage
		output_audio	: out	std_logic_vector(11 downto 0) -- Leads to next effect stage
	);

end entity;

architecture rtl of LinearGain is

	-- Build an enumerated type for the state machine
	type state_type is (idle, es1, done);

	-- Register to hold the current state
	signal state   : state_type;

begin

	-- Logic to advance to the next state
	process (clk, reset, clear, input_audio)
		variable reg1, reg2	:	std_logic_vector(11 downto 0) := (others => '0');
		variable xorvalue, data_internal		:	std_logic_vector(11 downto 0);
	begin
		if (clear = '0') then
			state <= idle;
			data_internal := input_audio;
			output_audio <= input_audio;
		elsif (rising_edge(clk)) then
			case state is
				when idle=>
					output_audio <= data_internal;
					if (load_in = '1') then
						if (disable = '0') then
							reg1 := effectReg1; -- Just read them for now
							reg2 := effectReg2; -- Acts like a latch
							data_internal := input_audio; -- Set audio register
							state <= es1; -- Move to effect state
						else
							data_internal := input_audio; -- Set audio register
							output_audio <= data_internal;
							state <= done;
						end if;
					else
						-- data_internal := input_audio; -- Set audio register
						state <= idle;
					end if;
				when es1=>
				  if (reg1 = "000000000000") then -- If unitialized, xor with 1's
					 xorvalue := data_internal xor "111111111111";
					 data_internal := xorvalue;
					else
					 xorvalue := data_internal xor reg1;
					 data_internal := xorvalue;
					end if;
					output_audio <= data_internal;
					state <= done; -- Move to done state
				when done=>
					output_audio <= data_internal;
					if (reset = '0') then
						state <= idle; -- Go back to idle state
					else
						state <= done;
					end if;
			end case;
		end if;
	end process;

	-- Output depends solely on the current state
	process (state)
	begin
		case state is
			when idle =>
				load_out <= '0';
			when es1 =>
				load_out <= '0';
			when done =>
				load_out <= '1';
		end case;
	end process;

end rtl;
