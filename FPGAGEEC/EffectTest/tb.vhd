
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test is
end test;

architecture TB of test is
	
	component signed_multiply
		generic (
			DATA_WIDTH : natural := 12
		);
		port (
			a	   : in signed ((DATA_WIDTH-1) downto 0);
			b	   : in signed ((DATA_WIDTH-1) downto 0);
			result  : out signed ((2*DATA_WIDTH-1) downto 0)
		);
	end component;

--    component effect1
--    port(
--		clk				: in	std_logic; -- Driven by master clock
--		input_audio		: in	std_logic_vector(11 downto 0); -- 12-bit audio data
--		reset	 			: in	std_logic; -- Clear all signals and go to idle
--		load_in			: in 	std_logic; -- Indicator to start applying this stage's effect
--		effectReg1		: in	std_logic_vector(11 downto 0); -- First effect register
--		effectReg2		: in	std_logic_vector(11 downto 0); -- Second effect register
--		load_out			: out std_logic; -- Indicator for next effect stage
--		output_audio	: out	std_logic_vector(11 downto 0) -- Leads to next effect stage
--	); end component;
    
    signal clk      		: std_logic := '0';
	 signal input_audio	: std_logic_vector(11 downto 0);
	 signal reset			: std_logic := '1';
	 signal load_in		: std_logic := '0';
	 signal effectReg1, effectReg2	: std_logic_vector(11 downto 0);
	 signal load_out		: std_logic;
	 signal output_audio	: std_logic_vector(11 downto 0);
    
	 signal mul1, mul2	: std_logic_vector(11 downto 0);
	 signal mul1S, mul2S	: signed(11 downto 0);
	 signal output			: std_logic_vector(23 downto 0);
	 signal outputS		: signed(23 downto 0);
begin

    clk <= not clk after 1 ns;
--    uut: effect1 PORT MAP (
--            clk => clk,
--				input_audio => input_audio,
--				reset => reset,
--				load_in => load_in,
--				effectReg1 => effectReg1,
--				effectReg2 => effectReg2,
--				load_out => load_out,
--            output_audio => output_audio
--        );

	uut: signed_multiply port map (
		a => mul1S,
		b => mul2S,
		result => outputS
	);
	
	mul1S <= signed(mul1);
	mul2S <= signed(mul2);
	output <= std_logic_vector(outputS);
	process
	begin
		mul1 <= "000000000011";
		mul2 <= "111111111111";
		wait for 10 ns;
		wait;
	end process;

end TB;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EffectTest is
	port (
		fuckme	:	inout	std_logic
	);
end EffectTest;

architecture rtl of EffectTest is
begin
	fuckme <= '1';
end rtl;

