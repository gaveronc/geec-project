-- SPI synchronizer
-- Ensures all control and audio data get latched
-- without any race conditions

LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SPI_Sync is
	port (
		audio_cs	:	in		std_logic;	-- Unsychronized signal
		ctrl_cs	:	in		std_logic;	-- Unsychronized signal
		clk		:	in		std_logic;	-- Synchronize to this clock signal
		sync		:	out	std_logic	-- Syncronized signal
	);
end SPI_Sync;

architecture rtl of SPI_Sync is
	-- Use some one-tick delay modules to synchronize the asynchronous chip select signals
	component OneTickDelay
		port (
			async	:	in		std_logic;	-- Unsychronized signal
			clk	:	in		std_logic;	-- Synchronize to this clock signal
			sync	:	out	std_logic	-- Syncronized signal
		);
	end component;
	
	type state_type is (idle, latched);
	signal state	:	state_type	:=	idle;
	
	signal audio_sync, ctrl_sync	:	std_logic;
begin
	
	audio_tick	:	OneTickDelay
		port map (
			async => audio_cs,
			clk => clk,
			sync => audio_sync
		);
	
	ctrl_tick	:	OneTickDelay
		port map (
			async => ctrl_cs,
			clk => clk,
			sync => ctrl_sync
		);
	
	process (clk)
	begin
		if (rising_edge(clk)) then
			case state is
				when idle =>
					if (audio_sync = '1' and ctrl_sync = '1') then
						state <= latched;
					else
						state <= idle;
					end if;
				when latched =>
					if (audio_sync = '0') then
						state <= idle;
					else
						state <= latched;
					end if;
			end case;
		end if;
	end process;
	
	process (state)
	begin
		case state is
			when idle =>
				sync <= '0';
			when latched =>
				sync <= '1';
		end case;
	end process;
	
end rtl;
