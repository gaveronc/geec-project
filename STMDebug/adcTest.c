#include "stm32f10x.h"
#include <stdint.h>
#include "adcTest.h"
/*
void adcInit(void)
{

	
	//Bit 9 (ADC1 clock enable)
	RCC->APB2ENR |= 1<<9;
	
	//Bit 4 (I/O Port C clock enable) for potentiometer
	RCC->APB2ENR |= 1<<4;
	//Set Port C register 0 to 5 to analog input mode and reset output state
	GPIOC->CRL &= ~0x00FFFFFF;
	//set sampling time for channel 10 to channel 15
	ADC1->SMPR1 |= 0x0003FFFF;
	//one conversion
	ADC1->SQR1 &= ~ADC_SQR1_L;
}

uint16_t adcRead(int channel){
	
	//reset channel
	ADC1->SQR3 &= 0;
	//set channel
	ADC1->SQR3 |= channel;
	//continuous conversion
	ADC1->CR2 |= 0x0001;
	
	//wait for conversion to finish
	while ((ADC1->SR & 0x2) != 0x2) {
	}
	
	return (ADC1->DR);			// Return data
}

*/


void ADC_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;//Enable ADC1 clock
	
	GPIOC->CRL &= ~(GPIO_CRL_MODE0 | GPIO_CRL_MODE1 |GPIO_CRL_MODE2 |GPIO_CRL_MODE3 |GPIO_CRL_MODE4 | GPIO_CRL_MODE5);
	GPIOC->CRL &= ~(GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2 | GPIO_CRL_CNF3 | GPIO_CRL_CNF4 | GPIO_CRL_CNF5);
	
	ADC1->CR2 |= ADC_CR2_ADON;//Turn on ADC1
	
	//ADC1->SMPR1 |= ADC_SMPR1_SMP10 | ADC_SMPR1_SMP11 | ADC_SMPR1_SMP12 | ADC_SMPR1_SMP13 |ADC_SMPR1_SMP14 |ADC_SMPR1_SMP15 ;
	
	ADC1->SQR1 &= ~ADC_SQR1_L;//Only one conversion in sequence
	ADC1->SQR3 &= 0x0;//Channel zero
}

void ADC_start(uint32_t channel)
{
	ADC1->SQR3 = channel;//Select channel to convert
	ADC1->CR2 |= ADC_CR2_ADON;//Start conversion
}

uint16_t ADC_read(void)
{
	while((ADC1->SR & ADC_SR_EOC) == 0); // Wait for EOC
	return ADC1->DR; // Return converted value
}

