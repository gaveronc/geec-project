/* GPIO bus from PB0 to PB15 */

#include "stm32f10x.h"
#include <stdint.h>
#include "parallel.h"

void gpioInit(void) {
	//Enable peripheral clock for Port B (Bit 3)
	RCC->APB2ENR |=  1<<3 ;
	
	//Port B -> Alternate function output Open-drain and Output mode [max speed of 50MHz)
	GPIOB->CRL |=0xFFFFFFFF;
	GPIOB->CRH |=0xFFFFFFFF;
	

}

uint16_t parallelComm(uint16_t audiodata, uint16_t datafromFPGA){

	
	//1st clock
	GPIOB->ODR = audiodata | receiveControl;
	//2nd clock
	GPIOB->ODR = audiodata | loadControl;
	//3rd clock
	GPIOB->ODR = audiodata | receiveControl;
	while(GPIOB->IDR & (1<<14)) 
	{
		
	}
	//4th clock
	datafromFPGA=(GPIOB->IDR & 0xFFF);
	//5th clock
	GPIOB->ODR = 0xFFF | resetControl;
	//6th clock
	GPIOB->ODR = 0xFFF | receiveControl;
	
	
}