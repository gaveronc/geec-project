#include "stm32f10x.h"
#include <stdint.h>
#include "SPI.h"

/*
This module is configured for the following settings:
CPOL=1
CPHA=0
Bidirectional Data
16-bit frames
MSB First
*/
void clockInit (void)
{ 
	//GPIOA enable (Bit 2)
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	//SPI1 enable (Bit 12)
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
}
void spiInit (void)
{
	GPIOA->CRL &= 0x000F0FFF;
	//MOSI,MISO,NSS,SCLK
	GPIOA->CRL |= 0xB4B03000;
	//set PA4(Chip Select) high
	GPIOA->ODR |= 1 << 3;
	//Reset SPI1 settings
	SPI1->CR1 = 0x00000000; 
	SPI1->CR2 = 0x00000000;
	
	//enbale slave management (Bit 9)
	SPI1->CR1 |= SPI_CR1_SSM;
	
	//Internal slave select (Bit 8)
	SPI1->CR1 |= SPI_CR1_SSI;
	
	//Master Selection (Bit 2)
	SPI1->CR1 |= SPI_CR1_MSTR;
	SPI1->CR1 |= SPI_CR1_DFF;
	
	//CPOL=1 (Bit 1)
	SPI1->CR1 |= SPI_CR1_CPOL;
	
	//SPI enable (Bit 6)
	SPI1->CR1 |= SPI_CR1_SPE;
}

uint16_t spiSend (uint16_t msg)
{
	GPIOA->ODR &= ~(1 << 3);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI1->SR & SPI_SR_TXE)){};
	SPI1->DR=msg;
	while(!(SPI1->SR & SPI_SR_RXNE)){};
	msg=SPI1->DR;
	
	GPIOA->ODR |= 1<<3;
	
	return msg;
		
}
