#include "stm32f10x.h"
#include <stdint.h>
#include "adcTest.h"
#include "dac.h"
#include "spi.h"

int main(void)
{
	int i;
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN; // Enable all GPIO clocks
	clockInit();
	ADC_init();
	dacInit();
	spiInit();
	
	while(1){
		ADC_start(10); // Start conversion on channel 10
		dacWrite(spiSend(ADC_read()) ^ 0xFFF); // Get and store converted value
		for (i = 0; i < 1000; i += 1){} // Wait
	}
}

	