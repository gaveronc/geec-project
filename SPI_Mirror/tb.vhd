----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07/08/2016 07:58:13 PM
-- Design Name: 
-- Module Name: tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb is
end tb;

architecture Behavioral of tb is
    component SPI_Mirror
    port (
	    clk         : in    std_logic;
        sclk        : in    std_logic;
        mosi           : in    std_logic;
        miso          : out    std_logic;
        cs_n        : in    std_logic;
        LED           : out   std_logic;
        reset_n         : in    std_logic;
        
        debug        : out std_LOGIC_VECTOR(7 downto 0)
    ); end component;
    
    signal clk      : std_logic := '0';
    signal sclk     : std_logic := '1';
    signal mosi     : std_logic := '0';
    signal miso     : std_logic;
    signal LED      : std_logic;
    signal cs_n     : std_logic := '1';
    signal debug    : std_logic_vector(7 downto 0);
    signal reset_n  : std_logic := '1';
    signal stimulus : std_logic_vector(7 downto 0) := "10101010";
    signal stimulus2: std_logic_vector(7 downto 0) := "00001111";
    
begin

    clk <= not clk after 1 ns;
    uut: SPI_Mirror PORT MAP (
            clk => clk,
            sclk => sclk,
            mosi => mosi,
            miso => miso,
            cs_n => cs_n,
            LED => LED,
            debug => debug,
            reset_n => reset_n
        );

    process
    begin
        
        reset_n <= '0';
        wait for 10 ns;
        reset_n <= '1';
        wait for 10 ns;
        
        cs_n <= '0';
        wait for 10 ns;
        for i in 7 downto 0 loop
            mosi <= stimulus(i);
            wait for 5 ns;
            sclk <= '0';
            wait for 10 ns;
            sclk <= '1';
            wait for 5 ns;
        end loop;
        sclk <= '1';
        wait for 5 ns;
        cs_n <= '1';
        wait for 10 ns;
        
        cs_n <= '0';
        wait for 10 ns;
        for i in 7 downto 0 loop
            mosi <= stimulus2(i);
            wait for 5 ns;
            sclk <= '0';
            wait for 10 ns;
            sclk <= '1';
            wait for 5 ns;
        end loop;
        cs_n <= '1';
        
        wait; -- End Test
    end process;

end Behavioral;




