#include "stm32f10x.h"
#include <stdint.h>
#include "SPI.h"

void LEDInit(void) {
	//LED is on GPIO bank C
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;
	//Mask pin 9, then set to push-pull output
	GPIOC->CRH &= 0xFFFFFF0F;
	GPIOC->CRH |= 0x3 << 4;
}

int main(void)
{
	uint8_t msg;
	
	clockInit();
	spiInit();
	LEDInit();
	
	//Unit test code
	for(msg = 1; ; msg += 1) {
		if (spiSend(msg) == msg - 1) {//If the data was mirrored
			GPIOC->ODR |= 1 << 9;//Turn on LED
		} else {
			GPIOC->ODR &= ~(1 << 9);//Turn off LED
		}
	}
	
}
