--------------------------------------
--SPI Mirror								--
--Written by Cameron Gaveronski		--
--June 29, 2016							--
--------------------------------------
--------------------------------------
--Mirrors the SPI input data with a	--
--single-transaction phase shift.	--
--It receives, then sends what it	--
--received on the last					--
--communication.							--
--------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity SPI_Mirror is

	port(
	   clk         : in    std_logic; -- System clock running at 50 MHz
		sclk		: in	std_logic; -- SPI clock
		mosi       	: in	std_logic; -- SPI pin
		miso      	: out	std_logic; -- SPI pin
		cs_n		: in	std_logic; -- Active low chip select
		LED   		: out   std_logic; -- Provides some visual feedback
		reset_n	 	: in	std_logic; -- Active low asynchronous reset
		oe			: out std_logic; -- Enable level shift IC
		
		debug		: out std_LOGIC_VECTOR(7 downto 0); -- A bunch of LEDs for debugging registers
		other_leds	: out std_logic_vector(1 downto 0) -- A couple on-board LEDs that get turned off
	);

end entity;

architecture rtl of SPI_Mirror is
    signal tx_reg   :   std_logic_vector(7 downto 0);
    signal rx_reg   :   std_logic_vector(7 downto 0);
    signal tx_load  :   std_logic;
    signal done     :   std_logic;
	
	type state_type is (s_init, s_init2, s_wait, s_wait2, s_load);
	signal state   : state_type := s_init;
	signal next_state : state_type := state;
	
	component SPI_Slave
	   PORT(
	        cs_n    : in    std_logic; -- Active low chip select
           mosi    : in    std_logic;
           miso    : out   std_logic;
           reset_n : in    std_logic; -- Asynchronous reset, active low
           sclk    : in    std_logic; -- SPI clock
           
           -- Internal signals
           tx_reg  : in    std_logic_vector(7 downto 0); -- Buffered SPI register
           rx_reg  : out   std_logic_vector(7 downto 0); -- Buffered SPI register
           tx_load : in    std_logic; -- This was causing problems, so it was disconnected
           done    : out   std_logic; -- Indicates SPI transaction has finished
			  
			  -- Debug signal
			   -- Currently reflecting the state of the internal shift register
			  debug_out : out std_logic_vector(7 downto 0)
	   );
	end component;
begin
	--Create our SPI module
	SPI: SPI_Slave port map (
			cs_n => cs_n,
			mosi => mosi,
			miso => miso,
			reset_n => reset_n,
			sclk => sclk,
			tx_reg => tx_reg,
			rx_reg => rx_reg,
			tx_load => tx_load,
			done => done,
			debug_out => debug
		);
	
	-- State Register
    SYNC_PROC: process (clk)
    begin
       if (clk'event and clk = '1') then
          if (reset_n = '0') then
             state <= s_init;
          else
             state <= next_state;
          end if;
       end if;
    end process;

   --MOORE State-Machine - Outputs based on state only
    OUTPUT_DECODE: process (state, rx_reg)
    begin
        -- Defaults
        tx_load <= '0'; -- Currently does nothing
        
        case state is
            when s_init =>
            when s_init2 =>
                tx_load <= '1'; -- 
            when s_wait => -- Do nothing
				when s_wait2 =>
            when s_load =>
                tx_load <= '1';
            when others =>
        end case;
    end process;
	 tx_reg <= rx_reg; -- Used to get latched by state machine
	
    NEXT_STATE_DECODE: process (state, done, reset_n)
    begin
       -- Declare default state for next_state to avoid latches
		 -- State machine currently exists as a template of sorts for future project
       next_state <= state;
		 if (reset_n = '0') then
			next_state <= s_init;
		 else
			 case (state) is
				 when s_init =>
					 next_state <= s_init2;
				 when s_init2 =>
					 next_state <= s_wait;
				 when s_wait =>
					 if (done = '1') then
						 next_state <= s_load;
					 end if;
				 when s_wait2 =>
					 if (done = '0') then
						 next_state <= s_wait;
					 end if;
				 when s_load =>
					 next_state <= s_wait2;
				 when others =>
					 next_state <= s_init;
			 end case;
		 end if;
    end process;
	
	process (rx_reg, reset_n)
	begin -- If we have alternating bits, turn on a light
	   if (rx_reg = "10101010") then
	       LED <= '0';
	   else
	       LED <= '1';
	   end if;
	end process;
	
	-- Turn off the other LEDs
	other_leds <= "11";
	oe <= '1';
end rtl;
