--------------------------------------
--SPI TX/RX Registers					--
--Written by Cameron Gaveronski		--
--May 7, 2016								--
--------------------------------------
--------------------------------------
--This module is configured for the	--
--following settings:					--
--CPOL = 1									--
--CPHA = 0									--
--Bidirectional Data						--
--8-bit frames							--
--MSB First									--
--------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SPIModule is
-- CPOL = 1, CPHA = 0, bidirectional, 8-bit frames, MSB first
	port(
		sclk		:	in				std_logic; -- Master drives the clock signal
		cs			:	in				std_logic; -- Active low
		tx_buf	:	in				std_logic_vector (7 downto 0); -- Output buffer
		tx			:	out			std_logic	:=	'0';	--Serial bit being sent
		rx_buf	:	buffer		std_logic_vector (7 downto 0); -- Input buffer
		rx			:	in 			std_logic;	-- Serial bit received
		bit16		:	out			std_logic	:=	'0';	-- Indicate 16 bits TX/RX
		reset		:	in				std_logic	--Async reset
	);

	signal tx_int	:	std_logic_vector (7 downto 0);
end entity;

architecture rtl of SPIModule is
begin
	process (sclk, reset)
		variable count	:	integer range 0 to 7 := 0;	-- Count 8 clock cycles
	begin
		if (reset = '1') then
			tx_int <= (others => '0');
			rx_buf <= (others => '0');
			bit16 <= '0';
			count := 0;
		else
			if (cs = '0') then -- Chip select, active low
				if (sclk = '1') then -- Receive data
					if (count = 0) then
						tx_int <= tx_buf;
					end if;
					bit16 <= '0'; -- Default value
					--Receiver
					rx_buf(6 downto 0) <= rx_buf(7 downto 1); -- Shift register
					rx_buf(7) <= rx;
					
					if (count = 7) then -- TX/RX done
						count := 0;
						bit16 <= '1';
					else
						count := count + 1;
					end if;
				else -- Load transmitter
					tx_int(7 downto 1) <= tx_int(6 downto 0); -- Shift register
					tx_int(0) <= '0';
				end if;
			else -- Chip not selected
				count := 0;
			end if;
		end if;
	end process;
	
	tx <= tx_int(7); -- Always point TX at MSB
	
end rtl;
