#ifndef __SPI_H__
#define __SPI_H__

#include "stm32f10x.h"
#include <stdint.h>

void clockInit(void);
void spiInit(void);
uint8_t spiSend (uint8_t);

#endif
