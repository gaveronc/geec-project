#include "stm32f10x.h"
#include <stdint.h>
#include "SPI.h"

/*
This module is configured for the following settings:
CPOL=1
CPHA=0
Bidirectional Data
8-bit frames
MSB First
*/
void clockInit (void)
{ 
	//GPIOA enable (Bit 2)
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	//SPI1 enable (Bit 12)
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
}
void spiInit (void)
{
	GPIOA->CRL &= 0x0000FFFF;
	//MOSI,MISO,NSS,SCLK
	GPIOA->CRL |= 0xB4B30000;
	//set PA4(Chip Select) high
	GPIOA->ODR |= 1 << 4;
	//Reset SPI1 settings
	SPI1->CR1 = 0x00000000; 
	SPI1->CR2 = 0x00000000;
	
	//enbale slave managemenet (Bit 9)
	SPI1->CR1 |= SPI_CR1_SSM;
	
	//Internal slave select (Bit 8)
	SPI1->CR1 |= SPI_CR1_SSI;
	
	//Master Selection (Bit 2)
	SPI1->CR1 |= SPI_CR1_MSTR;
	
	//CPOL=1 (Bit 1)
	SPI1->CR1 |= SPI_CR1_CPOL;
	
	//SPI enable (Bit 6)
	SPI1->CR1 |= SPI_CR1_SPE;
}

uint8_t spiSend (uint8_t msg)
{
	GPIOA->ODR &= ~(1 << 4);
	
	//When Transmit buffer is empty, send message to data register
	while(!(SPI1->SR & SPI_SR_TXE)){};
	SPI1->DR=msg;
	while(!(SPI1->SR & SPI_SR_RXNE)){};
	msg=SPI1->DR;
	
	GPIOA->ODR |= 1<<4;
	
	return msg;
		
}
