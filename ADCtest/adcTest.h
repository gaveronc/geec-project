#include "stm32f10x.h"
#include <stdint.h>
/*
void adcInit (void);
uint16_t adcRead (int);
*/

void ADC_init(void);
void ADC_start(uint32_t channel);
uint16_t ADC_read(void);

