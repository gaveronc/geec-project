#include "stm32f10x.h"
#include <stdint.h>
#include "adcTest.h"

int main(void)
{
	uint16_t adcvalues[6][20]; // Store ADC readings
	uint8_t i, j; // Count variable
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN; // Enable all GPIO clocks
	ADC_init();
	
	while(1){
		for (j=0; j<6; j++){
			for (i=0; i<20; i++)
			{
				ADC_start(j+10); // Start conversion on channel 10
				adcvalues[j][i] = ADC_read(); // Get and store converted value
			}
		}
	}
}

	